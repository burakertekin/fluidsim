/****************************************************************************
** Meta object code from reading C++ file 'GLWindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../include/GLWindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'GLWindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_GLWindow_t {
    QByteArrayData data[33];
    char stringdata[434];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_GLWindow_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_GLWindow_t qt_meta_stringdata_GLWindow = {
    {
QT_MOC_LITERAL(0, 0, 8), // "GLWindow"
QT_MOC_LITERAL(1, 9, 13), // "runSimulation"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 14), // "stopSimulation"
QT_MOC_LITERAL(4, 39, 15), // "resetSimulation"
QT_MOC_LITERAL(5, 55, 6), // "io_vol"
QT_MOC_LITERAL(6, 62, 4), // "io_h"
QT_MOC_LITERAL(7, 67, 7), // "io_visc"
QT_MOC_LITERAL(8, 75, 10), // "io_density"
QT_MOC_LITERAL(9, 86, 14), // "io_gasConstant"
QT_MOC_LITERAL(10, 101, 10), // "io_tension"
QT_MOC_LITERAL(11, 112, 12), // "io_threshold"
QT_MOC_LITERAL(12, 125, 13), // "io_relaxation"
QT_MOC_LITERAL(13, 139, 13), // "io_elasticity"
QT_MOC_LITERAL(14, 153, 14), // "setBoxFilePath"
QT_MOC_LITERAL(15, 168, 11), // "std::string"
QT_MOC_LITERAL(16, 180, 10), // "io_boxPath"
QT_MOC_LITERAL(17, 191, 16), // "setFluidFilePath"
QT_MOC_LITERAL(18, 208, 12), // "io_fluidPath"
QT_MOC_LITERAL(19, 221, 11), // "uiSetVolume"
QT_MOC_LITERAL(20, 233, 20), // "uiSetSmoothingLength"
QT_MOC_LITERAL(21, 254, 19), // "uisetViscosityCoeff"
QT_MOC_LITERAL(22, 274, 16), // "uisetRestDensity"
QT_MOC_LITERAL(23, 291, 16), // "uisetGasConstant"
QT_MOC_LITERAL(24, 308, 12), // "uisetTension"
QT_MOC_LITERAL(25, 321, 21), // "uisetSurfaceThreshold"
QT_MOC_LITERAL(26, 343, 15), // "uisetElasticity"
QT_MOC_LITERAL(27, 359, 15), // "uisetRelaxation"
QT_MOC_LITERAL(28, 375, 11), // "radioSelect"
QT_MOC_LITERAL(29, 387, 13), // "io_radioIndex"
QT_MOC_LITERAL(30, 401, 9), // "checkDraw"
QT_MOC_LITERAL(31, 411, 8), // "io_check"
QT_MOC_LITERAL(32, 420, 13) // "collisionCond"

    },
    "GLWindow\0runSimulation\0\0stopSimulation\0"
    "resetSimulation\0io_vol\0io_h\0io_visc\0"
    "io_density\0io_gasConstant\0io_tension\0"
    "io_threshold\0io_relaxation\0io_elasticity\0"
    "setBoxFilePath\0std::string\0io_boxPath\0"
    "setFluidFilePath\0io_fluidPath\0uiSetVolume\0"
    "uiSetSmoothingLength\0uisetViscosityCoeff\0"
    "uisetRestDensity\0uisetGasConstant\0"
    "uisetTension\0uisetSurfaceThreshold\0"
    "uisetElasticity\0uisetRelaxation\0"
    "radioSelect\0io_radioIndex\0checkDraw\0"
    "io_check\0collisionCond"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_GLWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   99,    2, 0x0a /* Public */,
       3,    0,  100,    2, 0x0a /* Public */,
       4,    9,  101,    2, 0x0a /* Public */,
      14,    1,  120,    2, 0x0a /* Public */,
      17,    1,  123,    2, 0x0a /* Public */,
      19,    1,  126,    2, 0x0a /* Public */,
      20,    1,  129,    2, 0x0a /* Public */,
      21,    1,  132,    2, 0x0a /* Public */,
      22,    1,  135,    2, 0x0a /* Public */,
      23,    1,  138,    2, 0x0a /* Public */,
      24,    1,  141,    2, 0x0a /* Public */,
      25,    1,  144,    2, 0x0a /* Public */,
      26,    1,  147,    2, 0x0a /* Public */,
      27,    1,  150,    2, 0x0a /* Public */,
      28,    1,  153,    2, 0x0a /* Public */,
      30,    1,  156,    2, 0x0a /* Public */,
      32,    1,  159,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double, QMetaType::Double, QMetaType::Double, QMetaType::Double, QMetaType::Double, QMetaType::Double, QMetaType::Double, QMetaType::Double, QMetaType::Double,    5,    6,    7,    8,    9,   10,   11,   12,   13,
    QMetaType::Void, 0x80000000 | 15,   16,
    QMetaType::Void, 0x80000000 | 15,   18,
    QMetaType::Void, QMetaType::Double,    5,
    QMetaType::Void, QMetaType::Double,    6,
    QMetaType::Void, QMetaType::Double,    7,
    QMetaType::Void, QMetaType::Double,    8,
    QMetaType::Void, QMetaType::Double,    9,
    QMetaType::Void, QMetaType::Double,   10,
    QMetaType::Void, QMetaType::Double,   11,
    QMetaType::Void, QMetaType::Double,   13,
    QMetaType::Void, QMetaType::Double,   12,
    QMetaType::Void, QMetaType::Int,   29,
    QMetaType::Void, QMetaType::Int,   31,
    QMetaType::Void, QMetaType::Int,   31,

       0        // eod
};

void GLWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        GLWindow *_t = static_cast<GLWindow *>(_o);
        switch (_id) {
        case 0: _t->runSimulation(); break;
        case 1: _t->stopSimulation(); break;
        case 2: _t->resetSimulation((*reinterpret_cast< const double(*)>(_a[1])),(*reinterpret_cast< const double(*)>(_a[2])),(*reinterpret_cast< const double(*)>(_a[3])),(*reinterpret_cast< const double(*)>(_a[4])),(*reinterpret_cast< const double(*)>(_a[5])),(*reinterpret_cast< const double(*)>(_a[6])),(*reinterpret_cast< const double(*)>(_a[7])),(*reinterpret_cast< const double(*)>(_a[8])),(*reinterpret_cast< const double(*)>(_a[9]))); break;
        case 3: _t->setBoxFilePath((*reinterpret_cast< const std::string(*)>(_a[1]))); break;
        case 4: _t->setFluidFilePath((*reinterpret_cast< const std::string(*)>(_a[1]))); break;
        case 5: _t->uiSetVolume((*reinterpret_cast< const double(*)>(_a[1]))); break;
        case 6: _t->uiSetSmoothingLength((*reinterpret_cast< const double(*)>(_a[1]))); break;
        case 7: _t->uisetViscosityCoeff((*reinterpret_cast< const double(*)>(_a[1]))); break;
        case 8: _t->uisetRestDensity((*reinterpret_cast< const double(*)>(_a[1]))); break;
        case 9: _t->uisetGasConstant((*reinterpret_cast< const double(*)>(_a[1]))); break;
        case 10: _t->uisetTension((*reinterpret_cast< const double(*)>(_a[1]))); break;
        case 11: _t->uisetSurfaceThreshold((*reinterpret_cast< const double(*)>(_a[1]))); break;
        case 12: _t->uisetElasticity((*reinterpret_cast< const double(*)>(_a[1]))); break;
        case 13: _t->uisetRelaxation((*reinterpret_cast< const double(*)>(_a[1]))); break;
        case 14: _t->radioSelect((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 15: _t->checkDraw((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 16: _t->collisionCond((*reinterpret_cast< const int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject GLWindow::staticMetaObject = {
    { &QGLWidget::staticMetaObject, qt_meta_stringdata_GLWindow.data,
      qt_meta_data_GLWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *GLWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *GLWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_GLWindow.stringdata))
        return static_cast<void*>(const_cast< GLWindow*>(this));
    return QGLWidget::qt_metacast(_clname);
}

int GLWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGLWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 17)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 17;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
