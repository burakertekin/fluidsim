#ifndef FORCE_H
#define FORCE_H

#include "Particle.h"
#include "Kernel.h"
#include "boost/foreach.hpp"
//------------------------------------------------------------------------------------------------------------
/// @class Force
/// @file Force.h
/// @author Burak Ertekin
/// @version 1.0
/// @date 15/08/15
/// Using Eigen/Dense for stress tensor matrix calculations, refer to following link for the library:
/// http://eigen.tuxfamily.org/index.php?title=Main_Page
//------------------------------------------------------------------------------------------------------------

/// @brief Force class to calculate the pressure, viscosity, gravity, surface tension forces
/// for Newtonian fluids & stress tensor and stress force calculations for Non-Newtonian
/// fluids.
class Force
{
public:
  //------------------------------------------------------------------------------------------------------------
  /// @brief ctor for Force class
  //------------------------------------------------------------------------------------------------------------
  Force();
  //------------------------------------------------------------------------------------------------------------
  /// @brief calculating pressure term
  /// @param [in] io_p, the current particle
  /// @param [in] io_n, the current neighbor particle
  /// @param [in] io_h, smoothing length
  /// @param [out] return pressure term
  //------------------------------------------------------------------------------------------------------------
  ngl::Vec3 getPressure(const Particle *io_p, const Particle *io_n, const float io_h);
  //------------------------------------------------------------------------------------------------------------
  /// @brief calculating viscosity term
  /// @param [in] io_p the current particle
  /// @param [in] io_n, the current neighbor particle
  /// @param [in] io_h, smoothing length
  /// @param [out] return viscosity term
  //------------------------------------------------------------------------------------------------------------
  ngl::Vec3 getViscosity(const Particle *io_p, const Particle *io_n, const float io_h);
  //------------------------------------------------------------------------------------------------------------
  /// @brief calculating stress term for Non-Newtonian - mao & yang 2005
  /// @param [in] io_p the current particle
  /// @param [in] io_n, the current neighbor particle
  /// @param [in] io_h, smoothing length
  /// @param [out] return stress term
  //------------------------------------------------------------------------------------------------------------
  ngl::Vec3 getStress(const Particle * io_p, const Particle *io_n, const float io_h);
  //------------------------------------------------------------------------------------------------------------
  /// @brief calculating surface tension - muller 2003
  /// @param [in] io_p, the current particle
  /// @param [in] io_n, the current neighbor particle
  /// @param [in] io_h, smoothing length
  /// @param [out] returns color field value
  //------------------------------------------------------------------------------------------------------------
  float getSurfaceTension(const Particle * io_p, const Particle * io_n, const float io_h);
  //------------------------------------------------------------------------------------------------------------
  /// @brief calculating surface normal - muller 2003
  /// @param [in] io_p, the current particle
  /// @param [in] io_n, the current neighbor particle
  /// @param [in] io_h, smoothing length
  /// @param [out] return surface normal
  //------------------------------------------------------------------------------------------------------------
  ngl::Vec3 getSurfaceNormal(const Particle * io_p, const Particle * io_n, const float io_h);
  //------------------------------------------------------------------------------------------------------------
  /// @brief calculating stress tensor - mao & yang 2005
  /// @param [in] io_p, the current particle
  /// @param [in] io_h, smoothing length
  /// @param [in] io_elasticity, the elastic constant
  /// @param [in] io_relaxation, relaxation time
  /// @param [in] io_dt, the timestep
  //------------------------------------------------------------------------------------------------------------
  void calculateStressTensor(Particle * & io_p, const float io_h, const float io_elasticity, const float io_relaxation, const float io_dt);
  //------------------------------------------------------------------------------------------------------------
  /// @brief calculating deformation and rotational tensor - mao & yang 2005
  /// @param [in] io_p, the current particle
  /// @param [in] io_h, smoothing length
  //------------------------------------------------------------------------------------------------------------
  void calculateTensors(const Particle *io_p, const float io_h);
  //------------------------------------------------------------------------------------------------------------
  /// @brief add gravity to external force
  /// @param [out] returns external force after adding gravity
  //------------------------------------------------------------------------------------------------------------
  ngl::Vec3 addGravity();
  //------------------------------------------------------------------------------------------------------------
  /// @brief get external force
  /// @param [out] returns external force
  //------------------------------------------------------------------------------------------------------------
  inline ngl::Vec3 getExternal() const {return m_external;}
  //------------------------------------------------------------------------------------------------------------
  /// @brief clear member variables
  //------------------------------------------------------------------------------------------------------------
  void clearForces();

private:
  //------------------------------------------------------------------------------------------------------------
  /// @brief member variables for the calculations
  //------------------------------------------------------------------------------------------------------------
  ngl::Vec3 m_pressure, m_viscosity, m_stress, m_external, m_normal;
  //------------------------------------------------------------------------------------------------------------
  /// @brief deformation and rotational tensor
  //------------------------------------------------------------------------------------------------------------
  Eigen::Matrix3f m_D, m_W;
  //------------------------------------------------------------------------------------------------------------
  /// @brief surface tension variable
  //------------------------------------------------------------------------------------------------------------
  float m_surface;
};

#endif // FORCE_H
