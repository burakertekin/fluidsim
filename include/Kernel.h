#ifndef KERNEL_H
#define KERNEL_H

#include "ngl/Vec3.h"
//------------------------------------------------------------------------------------------------------------
/// @class Kernel
/// @file Kernel.h
/// @author Burak Ertekin
/// @version 1.0
/// @date 16/08/15
/// Since this project was based on the work of Muller 2003 and Mao & Yang 2005,
/// implemented kernel functions are taken from these papers.
//------------------------------------------------------------------------------------------------------------

/// @brief Kernel class to calculate the different values of different kernel functions
class Kernel
{
public:
  //------------------------------------------------------------------------------------------------------------
  /// @brief ctor of Kernel class
  /// @param [in] io_r, vector between the particle and the neighbor
  /// @param [in] io_h, smoothing length
  //------------------------------------------------------------------------------------------------------------
  Kernel(const ngl::Vec3 io_r, const float io_h);
  //------------------------------------------------------------------------------------------------------------
  /// @brief pressure kernel. proposed by Desburn
  /// @param [out] returns gradient of Spiky kernel
  //------------------------------------------------------------------------------------------------------------
  ngl::Vec3 gradientSpiky();
  //------------------------------------------------------------------------------------------------------------
  /// @brief viscosity kernel, muller 2003' paper
  /// @param [out] returns laplacian of viscosity kernel
  //------------------------------------------------------------------------------------------------------------
  float viscoLaplacian();
  //------------------------------------------------------------------------------------------------------------
  /// @brief kernel functions from mao and yang' paper, used for density
  /// @param [out] returns default spline kernel
  //------------------------------------------------------------------------------------------------------------
  float spline();
  //------------------------------------------------------------------------------------------------------------
  /// @brief kernel functions from mao and yang' paper, used for stress tensor and term
  /// @param [out] returns gradient of spline kernel
  //------------------------------------------------------------------------------------------------------------
  ngl::Vec3 gradientSpline();
  //------------------------------------------------------------------------------------------------------------
  /// @brief functions from muller' paper, used for density
  /// @param [out] returns default poly6 kernel
  //------------------------------------------------------------------------------------------------------------
  float poly();
  //------------------------------------------------------------------------------------------------------------
  /// @brief functions from muller' paper, used for surface normal calculation
  /// @param [out] returns gradient of poly6 kernel
  //------------------------------------------------------------------------------------------------------------
  ngl::Vec3 gradientPoly();
  //------------------------------------------------------------------------------------------------------------
  /// @brief functions from muller' paper, used for surface tension calculation
  /// @param [out] returns laplacian of poly6 kernel
  //------------------------------------------------------------------------------------------------------------
  float laplacianPoly();

private:
  //------------------------------------------------------------------------------------------------------------
  /// @brief vector between the particle and the neighbor particle
  //------------------------------------------------------------------------------------------------------------
  ngl::Vec3 m_r;
  //------------------------------------------------------------------------------------------------------------
  /// @brief smoothing length
  //------------------------------------------------------------------------------------------------------------
  float m_h;
};

#endif // KERNEL_H
