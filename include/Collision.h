#ifndef COLLISION_H
#define COLLISION_H

#include "Scene.h"
#include "Particle.h"
#include "Integrator.h"
//------------------------------------------------------------------------------------------------------------
/// @class Collision
/// @file Collision.h
/// @author Burak Ertekin
/// @version 1.0
/// @date 15/08/15
//------------------------------------------------------------------------------------------------------------

/// @brief Collision class for both collision detection and reponse effects for the fluid.
/// Implemented only boundary and sphere object collision cases
class Collision
{
public:
  //------------------------------------------------------------------------------------------------------------
  /// @brief ctor for Collision class
  //------------------------------------------------------------------------------------------------------------
  Collision();
  //------------------------------------------------------------------------------------------------------------
  /// @brief using scene class as reference to "re-create" the same scene in this class
  /// @param [in] io_scene the input scene from fluid class
  //------------------------------------------------------------------------------------------------------------
  void setScene(Scene io_scene);
  //------------------------------------------------------------------------------------------------------------
  /// @brief collision detection and response with the simulation box
  /// @param [in] io_p the current particle, thats been check for collision
  //------------------------------------------------------------------------------------------------------------
  void checkWalls(Particle * & io_p);
  //------------------------------------------------------------------------------------------------------------
  /// @brief collision detection and response with the sphere object
  /// @param [in] io_p, the current particle
  /// @param [in] io_obstacles, the obstacle list
  /// @param [in] io_rad, the sphere radius
  //------------------------------------------------------------------------------------------------------------
  void checkSphere(Particle * & io_p, std::vector<ngl::Vec3> io_obstacles, float io_rad);
private:
  //------------------------------------------------------------------------------------------------------------
  /// @brief scene object to "re-create" the same scene
  Scene m_myScene;
  //------------------------------------------------------------------------------------------------------------
};

#endif // COLLISION_H

