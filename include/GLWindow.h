#ifndef GLWINDOW_H
#define GLWINDOW_H

#include <ngl/Camera.h>
#include <ngl/Transformation.h>
#include <QEvent>
#include <QResizeEvent>
#include <QGLWidget>
#include <ngl/Light.h>
#include "Fluid.h"
#include "Scene.h"
#include "ngl/VertexArrayObject.h"
#include "ngl/VAOPrimitives.h"
#include "QTime"
#include "ngl/Text.h"
//------------------------------------------------------------------------------------------------------------
/// @class GLWindow
/// @file GLWindow.h
/// @version 1.0
/// @date 15/08/15
/// @author Jon Macey
/// This class is based on Jon Macey's Mass Spring Demo
/// Used this demo as reference for mouse events, ui elements etc.
//------------------------------------------------------------------------------------------------------------

/// @brief GLWindow class is created to be the bridge between the structure and the visualization of the code
class GLWindow : public QGLWidget
{
Q_OBJECT        // must include this if you use Qt signals/slots
public :
  //------------------------------------------------------------------------------------------------------------
  /// @brief Constructor for GLWindow
  /// @param[in] _timer the time value for simulation updates
  /// @param [in] _parent the parent window to create the GL context in
  //------------------------------------------------------------------------------------------------------------
  GLWindow(const QGLFormat _format,QWidget *_parent );
  //------------------------------------------------------------------------------------------------------------
  /// @brief dtor
  //------------------------------------------------------------------------------------------------------------
  ~GLWindow();
  //------------------------------------------------------------------------------------------------------------
  /// @brief initialisation function
  //------------------------------------------------------------------------------------------------------------
  void initializeGL();
  //------------------------------------------------------------------------------------------------------------
  /// @brief painting function
  //------------------------------------------------------------------------------------------------------------
  void paintGL();
  //------------------------------------------------------------------------------------------------------------
  /// @brief initialize scene
  //------------------------------------------------------------------------------------------------------------
  void initScene();
  //------------------------------------------------------------------------------------------------------------
  /// @brief set the scene according to the input
  /// param [in] io_scene, input scene from fluid class
  //------------------------------------------------------------------------------------------------------------
  void setScene(Scene io_scene);
public slots:
  //------------------------------------------------------------------------------------------------------------
  /// @brief run the simulation
  //------------------------------------------------------------------------------------------------------------
  inline void runSimulation() {timer = startTimer(m_timer_value);}
  //------------------------------------------------------------------------------------------------------------
  /// @brief stop the simulation
  //------------------------------------------------------------------------------------------------------------
  inline void stopSimulation() {killTimer(timer);}
  //------------------------------------------------------------------------------------------------------------
  /// @brief reset the simulation and all the parameters along with it
  /// @param [in] io_vol, input volume from UI
  /// @param [in] io_h, input smoothing length from UI
  /// @param [in] io_visc, input viscosity coefficient from UI
  /// @param [in] io_density, input density from UI
  /// @param [in] io_gasConstant, input gas constant from UI
  /// @param [in] io_tension, input tension coefficient from UI
  /// @param [in] io_threshold, input threshold value from UI
  /// @param [in] io_relaxation, input relaxation time from UI
  /// @param [in] io_elasticity, input elastic coefficient from UI
  //------------------------------------------------------------------------------------------------------------
  void resetSimulation(const double io_vol, const double io_h, const double io_visc, const double io_density,
                       const double io_gasConstant, const double io_tension, const double io_threshold,
                       const double io_relaxation, const double io_elasticity);
  //------------------------------------------------------------------------------------------------------------
  /// @brief set box file path according to the string input
  /// @param [in] io_boxPath, string from the text edit in UI
  //------------------------------------------------------------------------------------------------------------
  inline void setBoxFilePath(const std::string io_boxPath) {m_myFluid.m_boxObjectPath = io_boxPath;}
  //------------------------------------------------------------------------------------------------------------
  /// @brief set fluid file path accordinf to the string input
  /// @param [in] io_fluidPath, string from the text edit in UI
  //------------------------------------------------------------------------------------------------------------
  inline void setFluidFilePath(const std::string io_fluidPath) {m_myFluid.m_fluidObjectPath = io_fluidPath;}
  //------------------------------------------------------------------------------------------------------------
  /// @brief set volume
  /// @param [in] io_vol, input volume from UI
  //------------------------------------------------------------------------------------------------------------
  inline void uiSetVolume(const double io_vol) {m_myFluid.setVolume(io_vol);}
  //------------------------------------------------------------------------------------------------------------
  /// @brief set smoothing length
  /// @param [in] io_h, input smoothing length from UI
  //------------------------------------------------------------------------------------------------------------
  inline void uiSetSmoothingLength(const double io_h) {m_myFluid.setSmoothingLength(io_h);}
  //------------------------------------------------------------------------------------------------------------
  /// @brief set viscosity coefficient
  /// @param [in] io_visc, input viscosity coefficient from UI
  //------------------------------------------------------------------------------------------------------------
  inline void uisetViscosityCoeff(const double io_visc) {m_myFluid.setViscosityCoeff(io_visc);}
  //------------------------------------------------------------------------------------------------------------
  /// @brief set density
  /// @param [in] io_density, input density from UI
  //------------------------------------------------------------------------------------------------------------
  inline void uisetRestDensity(const double io_density) {m_myFluid.setRestDensity(io_density);}
  //------------------------------------------------------------------------------------------------------------
  /// @brief set gas constant
  /// @param [in] io_gasConstant, input gas constant from UI
  //------------------------------------------------------------------------------------------------------------
  inline void uisetGasConstant(const double io_gasConstant) {m_myFluid.setGasConstant(io_gasConstant);}
  //------------------------------------------------------------------------------------------------------------
  /// @brief set tension coefficient
  /// @param [in] io_tension, input tension from UI
  //------------------------------------------------------------------------------------------------------------
  inline void uisetTension(const double io_tension) {m_myFluid.setTension(io_tension);}
  //------------------------------------------------------------------------------------------------------------
  /// @brief set threshold value
  /// @param [in] io_threshold, input threshold value from UI
  //------------------------------------------------------------------------------------------------------------
  inline void uisetSurfaceThreshold(const double io_threshold) {m_myFluid.setSurfaceThreshold(io_threshold);}
  //------------------------------------------------------------------------------------------------------------
  /// @brief set elasticity coefficient
  /// @param [in] io_elasticity, input elasticity from UI
  //------------------------------------------------------------------------------------------------------------
  inline void uisetElasticity(const double io_elasticity) {m_myFluid.setElasticity(io_elasticity);}
  //------------------------------------------------------------------------------------------------------------
  /// @brief set relaxation time
  /// @param [in] io_relaxation, input relaxation time from UI
  //------------------------------------------------------------------------------------------------------------
  inline void uisetRelaxation(const double io_relaxation) {m_myFluid.setRelaxation(io_relaxation);}
  //------------------------------------------------------------------------------------------------------------
  /// @brief set fluid type
  /// @param [in] io_radioIndex, radio button input for fluid type
  //------------------------------------------------------------------------------------------------------------
  void radioSelect(const int io_radioIndex);
  //------------------------------------------------------------------------------------------------------------
  /// @brief check whether to draw the scene or not
  /// @param [in] io_check, checkbox input
  //------------------------------------------------------------------------------------------------------------
  void checkDraw(const int io_check);
  //------------------------------------------------------------------------------------------------------------
  /// @brief check whether to draw and create collision condition for the fluid
  /// @param [in] io_check, checkbox input
  //------------------------------------------------------------------------------------------------------------
  void collisionCond(const int io_check);

private:
  //------------------------------------------------------------------------------------------------------------
  /// @brief the timer
  //------------------------------------------------------------------------------------------------------------
  int timer;
  //------------------------------------------------------------------------------------------------------------
  /// @brief another timer value for run/stop/reset functions
  //------------------------------------------------------------------------------------------------------------
  int m_timer_value;
  //------------------------------------------------------------------------------------------------------------
  /// @brief used to store the x rotation mouse value
  //------------------------------------------------------------------------------------------------------------
  int m_spinXFace;
  //------------------------------------------------------------------------------------------------------------
  /// @brief used to store the y rotation mouse value
  //------------------------------------------------------------------------------------------------------------
  int m_spinYFace;
  //------------------------------------------------------------------------------------------------------------
  /// @brief flag to indicate if the mouse button is pressed when dragging
  //------------------------------------------------------------------------------------------------------------
  bool m_rotate;
  //------------------------------------------------------------------------------------------------------------
  /// @brief flag to indicate if the right mouse button is pressed when dragging
  //------------------------------------------------------------------------------------------------------------
  bool m_translate;
  //------------------------------------------------------------------------------------------------------------
  /// @brief the previous x mouse value
  //------------------------------------------------------------------------------------------------------------
  int m_origX;
  //------------------------------------------------------------------------------------------------------------
  /// @brief the previous y mouse value
  //------------------------------------------------------------------------------------------------------------
  int m_origY;
  //------------------------------------------------------------------------------------------------------------
  /// @brief the previous x mouse value for position changes
  //------------------------------------------------------------------------------------------------------------
  int m_origXPos;
  //------------------------------------------------------------------------------------------------------------
  /// @brief the previous y mouse value for position changes
  //------------------------------------------------------------------------------------------------------------
  int m_origYPos;
  //------------------------------------------------------------------------------------------------------------
  /// @brief used to store the global mouse transforms
  //------------------------------------------------------------------------------------------------------------
  ngl::Mat4 m_mouseGlobalTX;
  //------------------------------------------------------------------------------------------------------------
  /// @brief our camera
  //------------------------------------------------------------------------------------------------------------
  ngl::Camera *m_cam;
  //------------------------------------------------------------------------------------------------------------
  /// @brief the transformation stack for the gl transformations etc
  //------------------------------------------------------------------------------------------------------------
  ngl::Transformation m_transform;
  //------------------------------------------------------------------------------------------------------------
  /// @brief the model position for mouse movement
  //------------------------------------------------------------------------------------------------------------
  ngl::Vec3 m_modelPos;
  //------------------------------------------------------------------------------------------------------------
  /// @brief current time value to calculate framerate
  //------------------------------------------------------------------------------------------------------------
  QTime m_currentTime;
  //------------------------------------------------------------------------------------------------------------
  /// @brief our text
  //------------------------------------------------------------------------------------------------------------
  ngl::Text *m_text;
  //------------------------------------------------------------------------------------------------------------
  /// @brief the sphere primitive for collision objects
  //------------------------------------------------------------------------------------------------------------
  ngl::VAOPrimitives *m_sphere;
  //------------------------------------------------------------------------------------------------------------
  /// @brief this method is called every time a mouse is moved
  /// @param _event the Qt Event structure
  //------------------------------------------------------------------------------------------------------------
  void mouseMoveEvent (QMouseEvent * _event);
  //------------------------------------------------------------------------------------------------------------
  /// @brief this method is called every time the mouse button is pressed
  /// inherited from QObject and overridden here.
  /// @param _event the Qt Event structure
  //------------------------------------------------------------------------------------------------------------
  void mousePressEvent (QMouseEvent *_event);
  //------------------------------------------------------------------------------------------------------------
  /// @brief this method is called everytime the mouse button is released
  /// inherited from QObject and overridden here.
  /// @param _event the Qt Event structure
  //------------------------------------------------------------------------------------------------------------
  void mouseReleaseEvent (QMouseEvent *_event);
  //------------------------------------------------------------------------------------------------------------
  /// @brief Qt Event called when a key is pressed
  /// @param [in] _event the Qt event to query for size etc
  //------------------------------------------------------------------------------------------------------------
  void keyPressEvent(QKeyEvent *_event);
  //------------------------------------------------------------------------------------------------------------
  /// @brief timer event trigered by startTimer
  //------------------------------------------------------------------------------------------------------------
  void timerEvent(QTimerEvent *_event);
  //------------------------------------------------------------------------------------------------------------
  /// @brief this method is called everytime the mouse wheel is moved
  /// inherited from QObject and overridden here.
  /// @param _event the Qt Event structure
  //------------------------------------------------------------------------------------------------------------
  void wheelEvent( QWheelEvent *_event);
  //------------------------------------------------------------------------------------------------------------
  /// @brief get rotation angle, rotate y axis to normal
  //------------------------------------------------------------------------------------------------------------
  ngl::Vec3 getRotationFromY(ngl::Vec3 _vec) const;
  //------------------------------------------------------------------------------------------------------------
  /// @brief booleans to check if drawing, and fluid type radio
  //------------------------------------------------------------------------------------------------------------
  bool m_checkDraw, m_fluidType;
  //------------------------------------------------------------------------------------------------------------
  /// @brief draw the particles
  /// @param [in] io_pos, input position
  /// @param [in] io_rad, input radius
  /// @param [in] io_c, input colour
  //------------------------------------------------------------------------------------------------------------
  void drawParticles(ngl::Vec3 io_pos, float io_rad, ngl::Colour io_c);
  //------------------------------------------------------------------------------------------------------------
  /// @brief draw the scene
  //------------------------------------------------------------------------------------------------------------
  void drawScene();
  //------------------------------------------------------------------------------------------------------------
  /// @brief draw the sphere obstacles
  /// @param [in] io_obstacle, obstacle coordinate
  //------------------------------------------------------------------------------------------------------------
  void drawSpheres(ngl::Vec3 io_obstacle);
  //------------------------------------------------------------------------------------------------------------
  /// @brief fluid object
  //------------------------------------------------------------------------------------------------------------
  Fluid m_myFluid;
  //------------------------------------------------------------------------------------------------------------
  /// @brief scene object
  //------------------------------------------------------------------------------------------------------------
  Scene m_myScene;
};

#endif // GLWINDOW_H
