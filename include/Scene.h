#ifndef SCENE_H
#define SCENE_H

#include "Particle.h"
#include "boost/foreach.hpp"
#include "ngl/Obj.h"
//------------------------------------------------------------------------------------------------------------
/// @class Scene
/// @file Scene.h
/// @author Burak Ertekin
/// @version 1.0
/// @date 17/08/15
//------------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------------
/// @brief defined a wall structure to keep the position, the normal and drawing boolean data
//------------------------------------------------------------------------------------------------------------
typedef struct WALL{
    ngl::Vec3   centre;
    float   size;
    float   a;
    float   b;
    float   c;
    float   d;
    bool    draw_flag;
} Wall;

//------------------------------------------------------------------------------------------------------------
/// @brief a simple cube class to act as a bounding box for octree and other applications in this code
//------------------------------------------------------------------------------------------------------------
class Cube
{
public:
  float m_minx, m_maxx, m_miny, m_maxy, m_minz, m_maxz;
};

/// @brief Scene class which creates the scene for the simulation. The scene elements (simulation box in this case)
/// are retrieved from Jon Macey's OctreeAbstract Demo. Refer to following link for reference:
/// https://github.com/NCCA/OctreeAbstract
class Scene
{
public:
  //------------------------------------------------------------------------------------------------------------
  /// @brief initialisation function for scene class
  //------------------------------------------------------------------------------------------------------------
  void init();
  //------------------------------------------------------------------------------------------------------------
  /// @brief set the simulation box according to given .obj input
  /// @param [in] io_obj, input obj
  //------------------------------------------------------------------------------------------------------------
  void setSimulationBox(ngl::Obj * io_obj);
  //------------------------------------------------------------------------------------------------------------
  /// @brief adding wall function, the walls are designed to be squared shaped
  /// @param [in] io_point, position of the wall
  /// @param [in] io_size, size of the wall
  /// @param [in] io_normal, normal vector of the wall
  /// @param [in] io_draw, boolean to decide whether to draw or not
  //------------------------------------------------------------------------------------------------------------
  void addWall(ngl::Vec3 io_point, float io_size, ngl::Vec3 io_normal, bool io_draw);
  //------------------------------------------------------------------------------------------------------------
  /// @brief clean the wall list
  //------------------------------------------------------------------------------------------------------------
  void clearWalls();
  //------------------------------------------------------------------------------------------------------------
  /// @brief get wall list
  /// @param [out] returns wall list
  //------------------------------------------------------------------------------------------------------------
  inline std::vector <Wall *> getWalls() const {return m_walls;}
  //------------------------------------------------------------------------------------------------------------
  /// @brief gets bounding box, simulation box of the scene
  /// @param [out] returns bounding box
  //------------------------------------------------------------------------------------------------------------
  inline Cube getBBCube() const {return m_simBox;}
private:
  //------------------------------------------------------------------------------------------------------------
  /// @brief wall list
  //------------------------------------------------------------------------------------------------------------
  std::vector <Wall *> m_walls;
  //------------------------------------------------------------------------------------------------------------
  /// @brief simulation box of the scene
  //------------------------------------------------------------------------------------------------------------
  Cube m_simBox;
};

#endif // SCENE_H

