#ifndef SPATIALHASHING_H
#define SPATIALHASHING_H

#include "Particle.h"
#include "map"
#include "boost/foreach.hpp"
#include "Scene.h"
//------------------------------------------------------------------------------------------------------------
/// @class SpatialHashing
/// @file SpatialHashing.h
/// @author Burak Ertekin
/// This class is written based on the paper of Teschner 2003. However it also contains in influences
/// of Kelager's (2006) and Priscott's (2010) work aswell. Modulus calculation and find the smallest
/// next prime number algorithm is retrieved from various sources which are cited in the functions
//------------------------------------------------------------------------------------------------------------

/// @brief SpatialHashing class is where neighbor searching is done
class SpatialHashing
{
public:
  //------------------------------------------------------------------------------------------------------------
  /// @brief ctor of SpatialHashing class
  /// @param [in] io_partCount, particle count of the fluid object
  /// @param [in] io_h, smoothing length
  //------------------------------------------------------------------------------------------------------------
  SpatialHashing(const int io_partCount, const float io_h);
  //------------------------------------------------------------------------------------------------------------
  /// @brief dtor
  //------------------------------------------------------------------------------------------------------------
  ~SpatialHashing();
  //------------------------------------------------------------------------------------------------------------
  /// @brief find the neighboring particles
  /// @param [in] io_p, current particle
  //------------------------------------------------------------------------------------------------------------
  void findNeighbors(Particle * & io_p);
  //------------------------------------------------------------------------------------------------------------
  /// @brief search through the hashmap
  /// @param [in] io_p, current particle
  /// @param [in] io_key, key value for hash index
  //------------------------------------------------------------------------------------------------------------
  void searchMap(Particle * & io_p, const int io_key);
  //------------------------------------------------------------------------------------------------------------
  /// @brief check if the neighbor candidate is already in the list
  /// @param [in] io_id, input id to be checked
  /// @param [in] io_neighborList, the neighborList to be checked whether it contains the given ID
  /// @param [in] io_key, key value for hash index
  /// @param [out] returns true if the given ID is in the neighbor list
  //------------------------------------------------------------------------------------------------------------
  bool checkList(const int io_key);
  //------------------------------------------------------------------------------------------------------------
  /// @brief fill the hashmap
  /// @param [in] io_particles, complete particle list to fill it in the map
  //------------------------------------------------------------------------------------------------------------
  void fillHashmap(std::vector<Particle *> io_particles);
  //------------------------------------------------------------------------------------------------------------
  /// @brief clear the hashmap
  //------------------------------------------------------------------------------------------------------------
  inline void clearHashmap() { m_map.clear(); }
  //------------------------------------------------------------------------------------------------------------
  /// @brief get the hash index key according to the given position
  /// @param [in] io_pos, input position
  //------------------------------------------------------------------------------------------------------------
  int getKey(const ngl::Vec3 io_pos);
  //------------------------------------------------------------------------------------------------------------
  /// @brief take the modulus of given inputs
  /// @param [in] io_a, first input integer
  /// @param [in] io_b, second input integer
  //------------------------------------------------------------------------------------------------------------
  int mod(const int io_a, const int io_b);
  //------------------------------------------------------------------------------------------------------------
  /// @brief discretize the given position according to the cell size
  /// @param [in] io_pos, input position to be discretized
  /// @param [out] returns discretized position vector
  //------------------------------------------------------------------------------------------------------------
  ngl::Vec3 discretize(const ngl::Vec3 io_pos);
  //------------------------------------------------------------------------------------------------------------
  /// @brief checking whether the given input is prime number
  /// @param [in] io_num, num to be checked
  //------------------------------------------------------------------------------------------------------------
  bool isPrime(const int io_num);
  //------------------------------------------------------------------------------------------------------------
  /// @brief find the next smallest prime number to find a proper hash table size
  /// @param [in] io_num, num be checked
  //------------------------------------------------------------------------------------------------------------
  int nextPrime(int io_num);
private:
  //------------------------------------------------------------------------------------------------------------
  /// @brief defining hashmap as multimap for neighbor particles
  //------------------------------------------------------------------------------------------------------------
  typedef std::multimap<int, Particle *> hashmap;
  //------------------------------------------------------------------------------------------------------------
  /// @brief defining a hashmap for checkList function, using hashmap to check if the neighbor candidates
  /// instead iterating through a list
  //------------------------------------------------------------------------------------------------------------
  typedef std::map<int, int> ID_hashmap;
  //------------------------------------------------------------------------------------------------------------
  /// @brief hashmap for the neighbor list
  //------------------------------------------------------------------------------------------------------------
  hashmap m_map;
  //------------------------------------------------------------------------------------------------------------
  /// @brief hashmap for checklist function
  //------------------------------------------------------------------------------------------------------------
  ID_hashmap m_idMap;
  //------------------------------------------------------------------------------------------------------------
  /// @brief grid cell size
  //------------------------------------------------------------------------------------------------------------
  float m_cellSize;
  //------------------------------------------------------------------------------------------------------------
  /// @brief prime numbers and hash table size
  //------------------------------------------------------------------------------------------------------------
  int m_p1, m_p2, m_p3, m_n;
};

#endif // SPATIALHASHING_H
