#ifndef FLUID_H
#define FLUID_H

#include "Octree.h"
#include "Force.h"
#include "Kernel.h"
#include "Integrator.h"
#include "SpatialHashing.h"
#include "OutputFile.h"
#include "Scene.h"
#include "Collision.h"
#include "ngl/Obj.h"
//------------------------------------------------------------------------------------------------------------
/// @class Fluid
/// @file Fluid.h
/// @author Burak Ertekin
/// @version 1.0
/// @date 17/08/15
//------------------------------------------------------------------------------------------------------------

/// @brief Main solver class of this implementation. Initialisation, updating and reseting the fluid.
/// Contains the constant variables for the simulation of the fluid, contains particles vector which
/// represents the fluid. Connection between UI elements aswell.
class Fluid
{
public:
  //------------------------------------------------------------------------------------------------------------
  /// @brief ctor of Fluid class
  //------------------------------------------------------------------------------------------------------------
  Fluid();
  //------------------------------------------------------------------------------------------------------------
  /// @brief dtor
  //------------------------------------------------------------------------------------------------------------
  ~Fluid();
  //------------------------------------------------------------------------------------------------------------
  /// @brief clear particle list
  //------------------------------------------------------------------------------------------------------------
  void clearParticles();
  //------------------------------------------------------------------------------------------------------------
  /// @brief initialisation function
  //------------------------------------------------------------------------------------------------------------
  void init();
  //------------------------------------------------------------------------------------------------------------
  /// @brief update function for Newtonian fluids
  //------------------------------------------------------------------------------------------------------------
  void update();
  //------------------------------------------------------------------------------------------------------------
  /// @brief reset function
  //------------------------------------------------------------------------------------------------------------
  void reset();
  //------------------------------------------------------------------------------------------------------------
  /// @brief update function for Non-Newtonian fluids
  //------------------------------------------------------------------------------------------------------------
  void updateNonNewtonian();
  //------------------------------------------------------------------------------------------------------------
  /// @brief calculate density for every particle. this function is a separate function because densities needs
  /// to be computed before everything since all SPH approximations are based on mass and density of each particle
  //------------------------------------------------------------------------------------------------------------
  void calculateDensity();
  //------------------------------------------------------------------------------------------------------------
  /// @brief get function for particle list
  /// @param [out] returns particle list
  //------------------------------------------------------------------------------------------------------------
  inline std::vector<Particle *> getParticles() const {return m_particles;}
  //------------------------------------------------------------------------------------------------------------
  /// @brief get function for iteration count
  /// @param [out] returns iteration count
  //------------------------------------------------------------------------------------------------------------
  inline int getItr() const {return m_itr;}
  //------------------------------------------------------------------------------------------------------------
  /// @brief get function for scene class for UI purposes
  /// @param [out] returns scene
  inline Scene getScene() const {return m_myScene;}
  //------------------------------------------------------------------------------------------------------------
  /// @brief set function for volume parameter
  /// @param [in] io_vol, input volume parameter from UI
  //------------------------------------------------------------------------------------------------------------
  inline void setVolume(const double io_vol) {m_volume = io_vol;}
  //------------------------------------------------------------------------------------------------------------
  /// @brief set function for smoothing length parameter
  /// @param [in] io_h, input smoothing length parameter from UI
  //------------------------------------------------------------------------------------------------------------
  inline void setSmoothingLength(const double io_h) {m_h = io_h;}
  //------------------------------------------------------------------------------------------------------------
  /// @brief set function for viscosity parameter
  /// @param [in] io_visc, input viscosity parameter from UI
  //------------------------------------------------------------------------------------------------------------
  inline void setViscosityCoeff(const double io_visc) {m_viscCoeff = io_visc;}
  //------------------------------------------------------------------------------------------------------------
  /// @brief set function for rest density parameter
  /// @param [in] io_density, input rest density parameter from UI
  //------------------------------------------------------------------------------------------------------------
  inline void setRestDensity(const double io_density) {m_restDensity = io_density;}
  //------------------------------------------------------------------------------------------------------------
  /// @brief set function for gas constant parameter
  /// @param [in] io_gasConstant, input gas constant parameter from UI
  //------------------------------------------------------------------------------------------------------------
  inline void setGasConstant(const double io_gasConstant) {m_gasConstant = io_gasConstant;}
  //------------------------------------------------------------------------------------------------------------
  /// @brief set function for tension coefficient
  /// @param [in] io_tension, input tension coefficient from UI
  //------------------------------------------------------------------------------------------------------------
  inline void setTension(const double io_tension) {m_tensionCoeff = io_tension;}
  //------------------------------------------------------------------------------------------------------------
  /// @brief set function for threshold value
  /// @param [in] io_threshold, input threshold value from UI
  //------------------------------------------------------------------------------------------------------------
  inline void setSurfaceThreshold(const double io_threshold) {m_surfaceThreshold = io_threshold;}
  //------------------------------------------------------------------------------------------------------------
  /// @brief set function for elasticity parameter
  /// @param [in] io_elasticity, input elasticity parameter from UI
  //------------------------------------------------------------------------------------------------------------
  inline void setElasticity(const double io_elasticity) {m_elasticity = io_elasticity;}
  //------------------------------------------------------------------------------------------------------------
  /// @brief set function for relaxation time parameter
  /// @param [in] io_relaxation, input relaxation time parameter from UI
  //------------------------------------------------------------------------------------------------------------
  inline void setRelaxation(const double io_relaxation) {m_relaxation = io_relaxation;}
  //------------------------------------------------------------------------------------------------------------
  /// @brief get function for obstacle list
  /// @param [out] returns obstacle list
  //------------------------------------------------------------------------------------------------------------
  inline std::vector<ngl::Vec3> getObstacles() const {return m_sphObstacles;}
  //------------------------------------------------------------------------------------------------------------
  /// @brief get function for obstacle sphere radius
  //------------------------------------------------------------------------------------------------------------
  inline float getObstacleRadius() const {return m_sphereRad;}
  //------------------------------------------------------------------------------------------------------------
  /// @brief set collision scenario according to the input coming from UI check box
  /// @param [in] io_condition, input condition from UI
  //------------------------------------------------------------------------------------------------------------
  inline void setCollisionCondition(const bool io_condition) {m_collisionCondition = io_condition;}
  //------------------------------------------------------------------------------------------------------------
  /// @brief get function for collision scenario
  /// @param [out] returns the whether there will be a collision or not
  //------------------------------------------------------------------------------------------------------------
  inline bool getCollisionCondition() const {return m_collisionCondition;}
  //------------------------------------------------------------------------------------------------------------
  /// @brief .obj file path names
  //------------------------------------------------------------------------------------------------------------
  std::string m_boxObjectPath, m_fluidObjectPath;

private:
  //------------------------------------------------------------------------------------------------------------
  /// @brief constant variables for simulation
  //------------------------------------------------------------------------------------------------------------
  float m_h, m_viscCoeff, m_elasticity, m_relaxation, m_tensionCoeff, m_volume,
    m_surfaceThreshold, m_gasConstant, m_restDensity, m_sphereRad;
  //------------------------------------------------------------------------------------------------------------
  /// @brief height variable of the octree, iteration count and idTrack for particle ID assigning
  //------------------------------------------------------------------------------------------------------------
  int m_height, m_itr, m_idTrack;
  //------------------------------------------------------------------------------------------------------------
  /// @brief .obj file objects of fluid and simulation box
  //------------------------------------------------------------------------------------------------------------
  ngl::Obj *m_fluidObject;
  ngl::Obj *m_simulationBox;
  //------------------------------------------------------------------------------------------------------------
  /// @brief boolean variables for UI elements
  //------------------------------------------------------------------------------------------------------------
  bool m_collisionCondition, m_writeCheck;
  //------------------------------------------------------------------------------------------------------------
  /// @brief fluid particle list. this list represents the fluid
  //------------------------------------------------------------------------------------------------------------
  std::vector <Particle *> m_particles;
  //------------------------------------------------------------------------------------------------------------
  /// @brief collision obstacle list. this list contains the sphere objects defined in initialisation function
  //------------------------------------------------------------------------------------------------------------
  std::vector <ngl::Vec3> m_sphObstacles;
  //------------------------------------------------------------------------------------------------------------
  /// @brief hash map object
  //------------------------------------------------------------------------------------------------------------
  SpatialHashing *m_hashMap;
  //------------------------------------------------------------------------------------------------------------
  /// @brief scene object, which contains scene elements
  //------------------------------------------------------------------------------------------------------------
  Scene m_myScene;
  //------------------------------------------------------------------------------------------------------------
  /// @brief integrator object that handles integration procedures
  //------------------------------------------------------------------------------------------------------------
  Integrator m_integrate;
  //------------------------------------------------------------------------------------------------------------
  /// @brief force object that handles force calculations and contains force variables
  //------------------------------------------------------------------------------------------------------------
  Force m_particleForces;
  //------------------------------------------------------------------------------------------------------------
  /// @brief collision object that handles collision procedures
  //------------------------------------------------------------------------------------------------------------
  Collision m_collision;
  //------------------------------------------------------------------------------------------------------------
  /// @brief output class that handles writing out the data to output file
  //------------------------------------------------------------------------------------------------------------
  OutputFile m_output;
};

#endif // FLUID_H
