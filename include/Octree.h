#ifndef OCTREE_H
#define OCTREE_H

#include "Particle.h"
#include "Scene.h"
#include "boost/foreach.hpp"
#include "ngl/Obj.h"
//------------------------------------------------------------------------------------------------------------
/// @class Octree
/// @file Octree.h
/// @author Xiaosong Yang modifed by Jon Macey and Burak Ertekin
/// @version 1.0
/// @date 16/08/15
//------------------------------------------------------------------------------------------------------------

/// @brief Octree class retrieved from Jon Macey's AbstractOctree Demo. Refer to following link for reference:
/// https://github.com/NCCA/OctreeAbstract
/// Finding neighbor algorithm adjusted to the needs of nearest neighbor search
class Octree
{
protected:
  //------------------------------------------------------------------------------------------------------------
  /// @brief tree node to hold parameters at each node of the tree, including a Cube boundary box,
  /// heigh of the node, objectList thats been held in the node, children treenodes(8 because its an octree)
  //------------------------------------------------------------------------------------------------------------
  class TreeNode
  {
  public:
    Cube m_limit;
    int m_height;
    std::vector<Particle *> m_objectList;
    TreeNode *m_child[8];
  };

public:
  //------------------------------------------------------------------------------------------------------------
  /// @brief ctor of Octree class
  /// @param [in] _height, height of the octree
  /// @param [in] _limit, bounding box of the tree
  //------------------------------------------------------------------------------------------------------------
  Octree(Cube _limit, int _height);
  //------------------------------------------------------------------------------------------------------------
  /// @brief dtor of Octree class
  //------------------------------------------------------------------------------------------------------------
  ~Octree();
  //------------------------------------------------------------------------------------------------------------
  /// @brief creating the tree according to the given bounding box and height parameters
  /// @param [in] _parent, parent node of the tree
  /// @param [in] _limit, surrounding bounding box of the tree
  /// @param [in] _height, the height of the tree
  //------------------------------------------------------------------------------------------------------------
  void createTree(TreeNode *io_parent, Cube io_limit, int io_height);
  //------------------------------------------------------------------------------------------------------------
  /// @brief deleting a tree node with all of its 8 children
  /// @param [in] _node, node to be deleted
  //------------------------------------------------------------------------------------------------------------
  void deleteTreeNode(TreeNode *io_node);
  //------------------------------------------------------------------------------------------------------------
  /// @brief adding a particle object to the tree
  /// @param [in] _p, current particle
  //------------------------------------------------------------------------------------------------------------
  void addObject(Particle *io_p);
  //------------------------------------------------------------------------------------------------------------
  /// @brief adding a particle to the given node as input
  /// @param [in] _node, the node where particle to be added
  /// @param [in] _p, the current particle
  //------------------------------------------------------------------------------------------------------------
  void addObjectToNode(TreeNode *io_node, Particle *io_p);
  //------------------------------------------------------------------------------------------------------------
  /// @brief checking if the _r is within the boundaries of the given bounding box
  /// @param [in] _pos, current position of the particle
  /// @param [in] _r, radius of the particle
  /// @param [in] _limit, boundary box to be checked
  /// @param [out] returns true if the position found inside of given bounding box
  //------------------------------------------------------------------------------------------------------------
  bool checkBounds(const ngl::Vec3 &io_pos, float io_r, const Cube &io_limit);
  //------------------------------------------------------------------------------------------------------------
  /// @brief checking if the given box is within the boundaries of the given bounding box
  /// @param [in] searchCube, given box
  /// @param [in] _limit, reference bounding box
  /// @param [out] return true if the cube is located inside of given bounding box
  //------------------------------------------------------------------------------------------------------------
  bool checkBounds(const Cube searchCube, const Cube io_limit);
  //------------------------------------------------------------------------------------------------------------
  /// @brief clear the tree from all the particles
  //------------------------------------------------------------------------------------------------------------
  void clearTree();
  //------------------------------------------------------------------------------------------------------------
  /// @brief clear the objects in the given node
  /// @param [in] _node, node to be cleared
  //------------------------------------------------------------------------------------------------------------
  void clearObjectFromNode(TreeNode *io_node);
  //------------------------------------------------------------------------------------------------------------
  /// @brief checking whether the given id is in the given neighbor list
  /// @param [in] _id, given id
  /// @param [in] _neighbor, neighbor list to be checked
  //------------------------------------------------------------------------------------------------------------
  bool inList(int io_id, std::vector<Particle *> io_neighbor);
  //------------------------------------------------------------------------------------------------------------
  /// @brief find neighbors of given particle _p
  /// @param [in] _p, current particle
  /// @param [in] _h, smoothing length
  //------------------------------------------------------------------------------------------------------------
  void findNeighbors(Particle *io_p, float io_h);
  //------------------------------------------------------------------------------------------------------------
  /// @brief find neighbors of given particle _p in that node _parent
  /// @param [in] _parent, node to be searched for neighbors
  /// @param [in] _p, current particle
  /// @param [in] _h, smoothing length
  //------------------------------------------------------------------------------------------------------------
  void findNeighborsInNode(TreeNode *io_parent, Particle *io_p, float io_h);
  //------------------------------------------------------------------------------------------------------------
  /// @brief forming a bounding box around the given position according to the smoothing length
  /// @param [in] _pos, given position
  /// @param [in] _h, smoothing length
  /// @param [out] returns the surrounding box of the particle according to the input
  //------------------------------------------------------------------------------------------------------------
  Cube fillSearchCube(const ngl::Vec3 io_pos, const float io_h);
protected:
  //------------------------------------------------------------------------------------------------------------
  /// @brief root node of the tree
  //------------------------------------------------------------------------------------------------------------
  TreeNode *m_root;
  //------------------------------------------------------------------------------------------------------------
  /// @brief surrounding bounding box of the tree
  //------------------------------------------------------------------------------------------------------------
  Cube m_searchCube;
  //------------------------------------------------------------------------------------------------------------
  /// @brief integer value to adjust the size of the bounding box of a given particle
  //------------------------------------------------------------------------------------------------------------
  int m_dist;
};


#endif // OCTREE_H
