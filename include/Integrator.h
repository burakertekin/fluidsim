#ifndef INTEGRATOR_H
#define INTEGRATOR_H

#include "Particle.h"
#include "Kernel.h"
#include "boost/foreach.hpp"
//------------------------------------------------------------------------------------------------------------
/// @class Integrator
/// @file Integrator.h
/// @author Burak Ertekin
/// @version 1.0
/// @date 16/08/15
/// Explicit integration methods implemented: leap-frog, explicit Euler
/// XSPH Velocity correction (Monaghan, 1989) implemented aswell. Using the reference of Paiva 2006
//------------------------------------------------------------------------------------------------------------

/// @brief Integrator class to calculate the new position and velocity values by integrating
/// the previous ones
class Integrator
{
public:
  //------------------------------------------------------------------------------------------------------------
  /// @brief ctor of Integrator class
  //------------------------------------------------------------------------------------------------------------
  Integrator();
  //------------------------------------------------------------------------------------------------------------
  /// @brief get the timestep. used in fluid class
  /// @param [out] return current timestep
  //------------------------------------------------------------------------------------------------------------
  inline float getTimestep() const {return m_dt;}
  //------------------------------------------------------------------------------------------------------------
  /// @brief set the timestep
  /// @param [in] io_timestep, input timestep
  //------------------------------------------------------------------------------------------------------------
  inline void setTimestep(const float io_timestep) {m_dt = io_timestep;}
  //------------------------------------------------------------------------------------------------------------
  /// @brief leap-frog integration
  /// @param [in] io_p, current particle
  //------------------------------------------------------------------------------------------------------------
  void leapFrog(Particle * & io_p);
  //------------------------------------------------------------------------------------------------------------
  /// @brief explicit euler integration
  /// @param [in] current particle
  //------------------------------------------------------------------------------------------------------------
  void explicitEuler(Particle * & io_p);
  //------------------------------------------------------------------------------------------------------------
  /// @brief semi implicit euler integration
  /// @param [in] io_p, current particle
  //------------------------------------------------------------------------------------------------------------
  void semiImplicitEuler(Particle * & io_p);
  //------------------------------------------------------------------------------------------------------------
  /// @brief XSPH velocity correction presented by Monaghan 1989
  /// @param [in] io_p, current particle
  /// @param [in] io_h, smoothing length
  /// @param [in] io_epsilon, velocity correction coefficient
  //------------------------------------------------------------------------------------------------------------
  void velocityCorrectionXSPH(Particle * & io_p, const float io_h, const float io_epsilon);
private:
  //------------------------------------------------------------------------------------------------------------
  /// @brief timestep variable
  //------------------------------------------------------------------------------------------------------------
  float m_dt;
};

#endif // INTEGRATOR_H
