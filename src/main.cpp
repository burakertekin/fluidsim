#include <QtGui/QGuiApplication>
#include "MainWindow.h"
#include <QApplication>
#include <iostream>

int main(int argc, char **argv)
{
  QApplication app(argc, argv);

  MainWindow window;

  window.resize(1000, 600);
  // and finally show
  window.show();

  return app.exec();
}
