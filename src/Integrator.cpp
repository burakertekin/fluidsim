#include "Integrator.h"
//------------------------------------------------------------------------------------------------------------
/// @file Integrator.cpp
/// @author Burak Ertekin
/// @version 1.0
/// @date 18/08/15
/// @brief Integrator class source. Integrating the acceleration to get new velocity then position values
/// of the particle. It contains three different integration methods: Explicit Euler, Semi Implicit Euler and
/// Leap Frog schemes. This class also has a velocity correction function introduced by Monaghan (1989)
//------------------------------------------------------------------------------------------------------------

#define SQR(x)		(x*x)


//ctor
Integrator::Integrator()
{
  m_dt = 0.009f;
  //m_dt = 0.003f;
  //m_dt = 0.01f;
}

//velocity correction approximation introduced by Monaghan
void Integrator::velocityCorrectionXSPH(Particle *&io_p, const float io_h, const float io_epsilon)
{
  ngl::Vec3 _correction;
  BOOST_FOREACH(Particle *_n, io_p->getNeighborList())
  {
    ngl::Vec3 _r = _n->getCurrPos() - io_p->getCurrPos();
    Kernel kernel(_r, io_h);
    float _W = kernel.poly();
    ngl::Vec3 _veloDiff = _n->getCurrVelo() - io_p->getCurrVelo();
    _correction += (_n->getMass() / (0.5*(io_p->getDensity()+_n->getDensity()))) * _veloDiff * _W;
  }
  ngl::Vec3 _newV = io_p->getCurrVelo() + _correction * io_epsilon;
  io_p->updateVelo(_newV);
}

//leap frog integration scheme
void Integrator::leapFrog(Particle * &io_p)
{
  //update the velo
  io_p->updateVelo(io_p->getLastVelo() + ((io_p->getLastAcc() + io_p->getCurrAcc())/2)*m_dt);

  //update the pos
  io_p->updatePos(io_p->getLastPos() + (io_p->getLastVelo() * m_dt) + ((io_p->getLastAcc()/2)*SQR(m_dt)));
}

//explicit euler integration scheme
void Integrator::explicitEuler(Particle * & io_p)
{
  //explicit euler integration
  ngl::Vec3 _tempPos = io_p->getCurrPos() + io_p->getCurrVelo()*m_dt;
  ngl::Vec3 _tempVelo = io_p->getCurrVelo() + io_p->getCurrAcc()*m_dt;
  io_p->setVelo(_tempVelo);
  io_p->setPos(_tempPos);
}

//semi implicit integration scheme
void Integrator::semiImplicitEuler(Particle *&io_p)
{
  ngl::Vec3 tempVelo = io_p->getCurrVelo() + io_p->getCurrAcc()*m_dt;
  io_p->updateVelo(tempVelo);

  ngl::Vec3 tempPos = io_p->getCurrPos() + io_p->getCurrVelo()*m_dt;
  io_p->updatePos(tempPos);
}
