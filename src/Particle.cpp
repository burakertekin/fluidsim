#include "Particle.h"
//------------------------------------------------------------------------------------------------------------
/// @file Particle.cpp
/// @author Burak Ertekin
/// @version 1.0
/// @date 18/08/15
/// @brief Particle class source.
//------------------------------------------------------------------------------------------------------------

//ctor
Particle::Particle()
{
  //assigning default values
  m_currPos.null();
  m_lastPos.null();
  m_lastVelo.null();
  m_currVelo.null();
  m_lastAcc.null();
  m_currAcc.null();
  m_mass = 1.0;
  m_density = 0.0;
  m_id = 0;
  m_radius = 0.05f;
  m_colour.set(0,0,1);
  m_neighbor.clear();
  m_scaPres = 0.0f;
  m_stressTensor.setZero(3,3);
}

Particle::~Particle()
{
  //dtor
  m_neighbor.clear();//clear the neighbor list
}

//updating position. assigning position to last position and input position to current position
void Particle::updatePos(ngl::Vec3 io_posIn)
{
  m_lastPos = m_currPos;
  m_currPos = io_posIn;
}

//updating velocity. assigning velocity to last velocity and input velocity to current velocity
void Particle::updateVelo(ngl::Vec3 io_veloIn)
{
  m_lastVelo = m_currVelo;
  m_currVelo = io_veloIn;
}

//updating acceleration. assigning acceleration to last acceleration and input acceleration to current acceleration
void Particle::updateAcc(ngl::Vec3 io_accIn)
{
  m_lastAcc = m_currAcc;
  m_currAcc = io_accIn;
}

//inserting the input particle to neighbor list
void Particle::pushToNeighborList(Particle * io_p)
{
  m_neighbor.push_back(io_p);
}
