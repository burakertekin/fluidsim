#include "OutputFile.h"
//------------------------------------------------------------------------------------------------------------
/// @file OutputFile.cpp
/// @author Burak Ertekin
/// @version 1.0
/// @date 18/08/15
/// @brief OutputFile class source. Writing out the data of the simulation to .txt files per iteration
//------------------------------------------------------------------------------------------------------------

//ctor
OutputFile::OutputFile()
{
  m_filename = "/transfer2/test12/txt2/output";
}

//dtor
OutputFile::~OutputFile()
{
  //the program is closed, write everything into the file now
  m_file.close();
}

//initialise, open the file to write
void OutputFile::init(int io_itr)
{
  std::string fileStepName = m_filename+ToString(io_itr)+".txt";
  m_file.open(fileStepName.c_str());
}

//finished, close the file
void OutputFile::finish()
{
  m_file.close();
}

//write the data per iteration
void OutputFile::writeStep(Particle *io_p)
{
  //<< " " << _p->getCurrVelo().m_x << " " << _p->getCurrVelo().m_y << " " << _p->getCurrVelo().m_z << " " <<  std::endl;
  m_file << io_p->getCurrPos().m_x << " " << io_p->getCurrPos().m_y << " " << io_p->getCurrPos().m_z << " " << std::endl;
  m_file.clear();
}

//convert int value to string
std::string OutputFile::ToString(int io_val)
{
    std::stringstream stream;
    stream << io_val;
    return stream.str();
}
