#include "Force.h"
//------------------------------------------------------------------------------------------------------------
/// @file Force.cpp
/// @author Burak Ertekin
/// @version 1.0
/// @date 18/08/15
/// @brief Force class source. Calculating all the force terms defined in Muller (2003) and Mao & Yang (2003)
/// papers. Stress tensor elements such as deformation tensor, rotational tensor are calculated here aswell.
//------------------------------------------------------------------------------------------------------------

#define SQR(x)		(x*x)

Force::Force()
{
  //all force values are initially set to null
  m_pressure.null();
  m_viscosity.null();
  m_external.null();
  m_stress.null();
  m_surface = 0;
  m_normal.null();
}

void Force::clearForces()
{
  //clearing all force values
  m_pressure.null();
  m_viscosity.null();
  m_external.null();
  m_stress.null();
  m_surface = 0;
  m_normal.null();
}

//get the stress term to include in the net force equation
ngl::Vec3 Force::getStress(const Particle * io_p, const Particle * io_n, const float io_h)
{
  ngl::Vec3 _r = io_p->getCurrPos() - io_n->getCurrPos();
  Kernel kernel(_r, io_h);
  float _volume = io_n->getMass() / (io_n->getDensity() * io_p->getDensity());

  //using the spline kernel in mao and yangs paper but gradient of it
  //ngl::Vec3 _W = kernel.gradientSpline();
  ngl::Vec3 _W = kernel.gradientPoly();

  //using the stress term calculation defined in Paiva' (2006) paper
  Eigen::Matrix3f _nTensor = io_n->getStressTensor();
  Eigen::Matrix3f _pTensor = io_p->getStressTensor();
  /*
  m_stress.m_x += _volume*((_nTensor(0,0)-_pTensor(0,0)*_W.m_x)+
                          (_nTensor(0,1)-_pTensor(0,1)*_W.m_y)+
                          (_nTensor(0,2)-_pTensor(0,2)*_W.m_z));
  m_stress.m_y += _volume*((_nTensor(1,0)-_pTensor(1,0)*_W.m_x)+
                          (_nTensor(1,1)-_pTensor(1,1)*_W.m_y)+
                          (_nTensor(1,2)-_pTensor(1,2)*_W.m_z));
  m_stress.m_z += _volume*((_nTensor(2,0)-_pTensor(2,0)*_W.m_x)+
                          (_nTensor(2,1)-_pTensor(2,1)*_W.m_y)+
                          (_nTensor(2,2)-_pTensor(2,2)*_W.m_z));
  */
  m_stress.m_x += _volume*((_nTensor(0,0)+_pTensor(0,0)*_W.m_x)+
                          (_nTensor(0,1)+_pTensor(0,1)*_W.m_y)+
                          (_nTensor(0,2)+_pTensor(0,2)*_W.m_z));
  m_stress.m_y += _volume*((_nTensor(1,0)+_pTensor(1,0)*_W.m_x)+
                          (_nTensor(1,1)+_pTensor(1,1)*_W.m_y)+
                          (_nTensor(1,2)+_pTensor(1,2)*_W.m_z));
  m_stress.m_z += _volume*((_nTensor(2,0)+_pTensor(2,0)*_W.m_x)+
                          (_nTensor(2,1)+_pTensor(2,1)*_W.m_y)+
                          (_nTensor(2,2)+_pTensor(2,2)*_W.m_z));

  return m_stress;
}

//calculating the stress tensor of the particle according to its deformation and rotational tensors
void Force::calculateStressTensor(Particle * & io_p, const float io_h, const float io_elasticity, const float io_relaxation, const float io_dt)
{
  //fill the deformation tensor and rotational tensor
  calculateTensors(io_p, io_h);

  Eigen::Matrix3f _I = Eigen::Matrix3f::Identity();

  //equation 13
  Eigen::Matrix3f _tracelessD = (m_D) - ((m_D.trace()/3.0)*_I);
  //getting the existing tensor of the particle
  Eigen::Matrix3f _tensor = io_p->getStressTensor();
  //equation 10
  Eigen::Matrix3f _sigma = 0.5*(_tensor*m_W - m_W*_tensor);

  //equation 9
  Eigen::Matrix3f dTdt = _sigma + io_elasticity*_tracelessD*0.5 - (1.0/io_relaxation)*_tensor;

  //obtaining the new tensor according to the previous one and the timestep
  _tensor = _tensor + dTdt * io_dt;

  //set the new tensor for the particle
  io_p->setStressTensor(_tensor);
}

//calculate the deformation and rotational tensors of the particle
void Force::calculateTensors(const Particle *io_p, const float io_h)
{
  //setting the tensors to zero initially
  m_D.setZero(3,3);
  m_W.setZero(3,3);
  //equation 16 temp values for 9 positions in the tensor
  float _dVxdRx, _dVydRx, _dVzdRx, _dVxdRy, _dVydRy, _dVzdRy, _dVxdRz, _dVydRz, _dVzdRz;
  _dVxdRx = _dVydRx = _dVzdRx = _dVxdRy = _dVydRy = _dVzdRy = _dVxdRz = _dVydRz = _dVzdRz = 0.0f;
  BOOST_FOREACH(Particle *_n, io_p->getNeighborList())
  {
    ngl::Vec3 _r = io_p->getCurrPos() - _n->getCurrPos();
    Kernel kernel(_r, io_h);
    //using the spline kernel in mao and yangs paper but gradient of it
    //ngl::Vec3 _W = kernel.gradientSpline();
    ngl::Vec3 _W = kernel.gradientPoly();

    //calculating equation 16
    ngl::Vec3 veloDiff = _n->getCurrVelo() - io_p->getCurrVelo();

    float _volume = _n->getMass() / _n->getDensity();

    _dVxdRx += _volume * veloDiff.m_x * _W.m_x;
    _dVydRx += _volume * veloDiff.m_y * _W.m_x;
    _dVzdRx += _volume * veloDiff.m_z * _W.m_x;
    _dVxdRy += _volume * veloDiff.m_x * _W.m_y;
    _dVydRy += _volume * veloDiff.m_y * _W.m_y;
    _dVzdRy += _volume * veloDiff.m_z * _W.m_y;
    _dVxdRz += _volume * veloDiff.m_x * _W.m_z;
    _dVydRz += _volume * veloDiff.m_y * _W.m_z;
    _dVzdRz += _volume * veloDiff.m_z * _W.m_z;
  }

  //equation 14 & 15, assigning values obtained from equation 16
  m_D(0,0) = 2*_dVxdRx;
  m_D(0,1) = _dVxdRy + _dVydRx;
  m_D(0,2) = _dVxdRz + _dVzdRx;
  m_D(1,0) = _dVydRx + _dVxdRy;
  m_D(1,1) = 2*_dVydRy;
  m_D(1,2) = _dVydRz + _dVzdRy;
  m_D(2,0) = _dVzdRx + _dVxdRz;
  m_D(2,1) = _dVzdRy + _dVydRz;
  m_D(2,2) = 2*_dVzdRz;

  //equation 11 & 12, assigning values obtained from equation 16
  m_W(0,0) = m_W(1,1) = m_W(2,2) = 0;
  m_W(0,1) = _dVxdRy - _dVydRx;
  m_W(0,2) = _dVxdRz - _dVzdRx;
  m_W(1,0) = _dVydRx - _dVxdRy;
  m_W(1,2) = _dVydRz - _dVzdRy;
  m_W(2,0) = _dVzdRx - _dVxdRz;
  m_W(2,1) = _dVzdRy - _dVydRz;

}

//calculate the surface normal of the particle
ngl::Vec3 Force::getSurfaceNormal(const Particle *io_p, const Particle *io_n, const float io_h)
{
  ngl::Vec3 _r = io_p->getCurrPos() - io_n->getCurrPos();
  Kernel kernel(_r, io_h);
  float _volume = io_n->getMass() / io_n->getDensity();

  //equation 16 in Muller' paper
  ngl::Vec3 _W = kernel.gradientPoly();
  m_normal += _W * _volume;
  return m_normal;
}

//calculate the surface tension of the particle
float Force::getSurfaceTension(const Particle *io_p, const Particle *io_n, const float io_h)
{
  ngl::Vec3 _r = io_p->getCurrPos() - io_n->getCurrPos();
  Kernel kernel(_r, io_h);
  float _W = kernel.laplacianPoly();
  float _volume = io_n->getMass() / io_n->getDensity();

  m_surface += _W * _volume;
  return m_surface;
}

//calculate the pressure term of the particle
ngl::Vec3 Force::getPressure(const Particle *io_p, const Particle *io_n, const float io_h)
{
  ngl::Vec3 _r = io_p->getCurrPos() - io_n->getCurrPos();
  Kernel kernel(_r, io_h);
  //use the spiky kernel stated in mao and yangs, muller' paper
  ngl::Vec3 _W = kernel.gradientSpiky();

  m_pressure += ((io_p->getScaPressure()/SQR((io_p->getDensity())))+(io_n->getScaPressure()/SQR((io_n->getDensity())))) *io_n->getMass() * _W;

  return m_pressure;
}

//calculate the viscosity term of the particle
ngl::Vec3 Force::getViscosity(const Particle *io_p, const Particle *io_n, const float io_h)
{
  ngl::Vec3 _r = io_p->getCurrPos() - io_n->getCurrPos();
  Kernel kernel(_r, io_h);
  //use the viscosity kernel specified in muller' 2003 paper
  float _W = kernel.viscoLaplacian();

  ngl::Vec3 _vDiff = io_n->getCurrVelo() - io_p->getCurrVelo();

  m_viscosity += _vDiff * (io_n->getMass()/io_n->getDensity()) * _W;
  return m_viscosity;
}

//add gravity to external force
ngl::Vec3 Force::addGravity()
{
  ngl::Vec3 gravity;
  gravity.set(0, -9.8, 0);
  m_external += gravity;
  return m_external;
}
