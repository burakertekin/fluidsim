#include "Octree.h"
//------------------------------------------------------------------------------------------------------------
/// @file Octree.cpp
/// @author Jon Macey and Burak Ertekin
/// @version 1.0
/// @date 18/08/15
/// @brief Octree implementation retrieved from Jon Macey' AbstractOctree Demo. Changed addObjectToNode and
/// neighbor searching function according to this project. This class is not being used.
//------------------------------------------------------------------------------------------------------------

//ctor
Octree::Octree(Cube _limit, int _height)
{
  m_root = new TreeNode;
  createTree(m_root, _limit, _height);
  m_dist = 3;
}

//dtor
Octree::~Octree()
{
  deleteTreeNode(m_root);
  delete m_root;
}

//delete tree node along with all its children
void Octree::deleteTreeNode(TreeNode *io_node)
{
  for(int i=0; i<8; ++i)
  {
    if(io_node->m_child[i]!=0)
    {
      deleteTreeNode(io_node->m_child[i]);
      delete io_node->m_child[i];
    }
  }
}

//create tree according to the parent node, and height
void Octree::createTree(TreeNode *io_parent, Cube io_limit, int io_height)
{
  io_parent->m_limit = io_limit;
  io_parent->m_objectList.clear();
  io_parent->m_height = io_height;
  io_height--;
  if(io_height == 0) // current level is leaves, no children
  {
    for(int i=0;i<8;++i)
    {
     io_parent->m_child[i] = 0;
    }
  }
  else
  {
    Cube childLimit;
    for(int i=0;i<8;++i)
    {
      io_parent->m_child[i] = new TreeNode;
      if(i%2==0) // i = 0, 2, 4, 6
      {
        childLimit.m_minx = io_limit.m_minx;
        childLimit.m_maxx = (io_limit.m_maxx+io_limit.m_minx)/2.0;
      }
      else // i = 1, 3, 5, 7
      {
        childLimit.m_minx = (io_limit.m_maxx+io_limit.m_minx)/2.0;
        childLimit.m_maxx = io_limit.m_maxx;
      }
      if(i==0 || i==1 || i==4 || i==5)
      {
        childLimit.m_miny = io_limit.m_miny;
        childLimit.m_maxy = (io_limit.m_maxy + io_limit.m_miny)/2.0;
      }
      else
      {
        childLimit.m_miny = (io_limit.m_maxy + io_limit.m_miny)/2.0;
        childLimit.m_maxy = io_limit.m_maxy;
      }
      if(i<4)
      {
        childLimit.m_minz = io_limit.m_minz;
        childLimit.m_maxz = (io_limit.m_maxz+io_limit.m_minz)/2.0;
      }
      else
      {
        childLimit.m_minz = (io_limit.m_maxz + io_limit.m_minz)/2.0;
        childLimit.m_maxz = io_limit.m_maxz;
      }
     createTree(io_parent->m_child[i], childLimit, io_height);
    } // end for
  }// end else
}

//check whether the input coordinate is in the given bounding box
bool Octree::checkBounds(const ngl::Vec3 &io_pos, ngl::Real io_r, const Cube &io_limit)
{
  return (io_pos.m_x-io_r >= io_limit.m_maxx ||
          io_pos.m_x+io_r <= io_limit.m_minx ||
          io_pos.m_y-io_r >= io_limit.m_maxy ||
          io_pos.m_y+io_r <= io_limit.m_miny ||
          io_pos.m_z-io_r >= io_limit.m_maxz ||
          io_pos.m_z+io_r <= io_limit.m_minz
          );
}

//check whether the input bounding box is in the given limiting bounding box
bool Octree::checkBounds(const Cube io_searchCube, const Cube io_limit)
{
  return (io_searchCube.m_maxx <= io_limit.m_maxx ||
          io_searchCube.m_minx >= io_limit.m_minx ||
          io_searchCube.m_maxy <= io_limit.m_maxy ||
          io_searchCube.m_miny >= io_limit.m_miny ||
          io_searchCube.m_maxz <= io_limit.m_maxz ||
          io_searchCube.m_minz >= io_limit.m_minz);
}

//add object to tree
void Octree::addObject(Particle *io_p)
{
  addObjectToNode(m_root, io_p);
}

//add object to a node
void Octree::addObjectToNode(TreeNode *io_node, Particle *io_p)
{
  if(io_node->m_height == 1) // this is the leaves level
  {
    io_node->m_objectList.push_back(io_p);
  }
  else
  {
    ngl::Vec3 pos = io_p->getCurrPos();
    float r = io_p->getRadius();
    Cube limit;
    for(int i=0;i<8;++i)
    {
      limit = io_node->m_child[i]->m_limit;
      if(checkBounds(pos,r,limit))
      {
        io_node->m_objectList.push_back(io_p);
        continue;
      }
      addObjectToNode(io_node->m_child[i], io_p);
    }
  }
}

//clear tree
void Octree::clearTree()
{
  clearObjectFromNode(m_root);
}

//clear object list from the node
void Octree::clearObjectFromNode(TreeNode *io_node)
{
  if(io_node->m_height == 1)
  {
    io_node->m_objectList.clear();
  }
  else
  {
    for(int i=0;i<8;++i)
    {
        clearObjectFromNode(io_node->m_child[i]);
    }
  }
}

//create a bounding box surrounding the given position
Cube Octree::fillSearchCube(const ngl::Vec3 io_pos, const float io_h)
{
  m_searchCube.m_maxx = io_pos.m_x+(m_dist*io_h);
  m_searchCube.m_maxy = io_pos.m_y+(m_dist*io_h);
  m_searchCube.m_maxz = io_pos.m_z+(m_dist*io_h);
  m_searchCube.m_minx = io_pos.m_x-(m_dist*io_h);
  m_searchCube.m_miny = io_pos.m_y-(m_dist*io_h);
  m_searchCube.m_minz = io_pos.m_z-(m_dist*io_h);
  return m_searchCube;
}

//check whether the input id is in given list
bool Octree::inList(int io_id, std::vector<Particle *> io_neighbor)
{
  BOOST_FOREACH(Particle *_n, io_neighbor)
  {
    if(io_id == _n->getID())
    {
      //if input id is in the std::vector return true
      return true;
    }
  }
  //default is not found
  return false;
}

//find neighbors of given particle in the tree
void Octree::findNeighbors(Particle *io_p, float io_h)
{
  findNeighborsInNode(m_root, io_p, io_h);
}

//find neighbors of given particle in a node
void Octree::findNeighborsInNode(TreeNode *io_parent, Particle *io_p, float io_h)
{
  BOOST_FOREACH(TreeNode *T, io_parent->m_child)
  {
    if(checkBounds(io_p->getCurrPos(), io_p->getRadius(), T->m_limit) && T->m_height>1 && !T->m_objectList.empty())
    {
      findNeighborsInNode(T, io_p, io_h);
    }
    else if(checkBounds(io_p->getCurrPos(), io_p->getRadius(), T->m_limit) && !T->m_objectList.empty())
    {
      BOOST_FOREACH(Particle *temp, T->m_objectList)
      {
        ngl::Vec3 checkPar;
        checkPar = temp->getCurrPos();
        ngl::Vec3 distance = io_p->getCurrPos() - temp->getCurrPos();
        //if(checkBounds(checkPar, 0, search))
        if(distance.length()<m_dist*io_h && distance.length()!=0)
        {
          if(!inList(temp->getID(), io_p->getNeighborList()))
          {
            io_p->pushToNeighborList(temp);
          }
        }

      }//for loop objectList
    }//else if
  }//for loop childList
}
