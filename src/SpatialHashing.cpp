#include "SpatialHashing.h"
//------------------------------------------------------------------------------------------------------------
/// @file SpatialHashing.cpp
/// @author Burak Ertekin
/// @version 1.0
/// @date 19/08/15
/// @brief SpatialHashing class source. This class is written based on the
/// paper of Teschner 2003. However it also contains in influences of Kelager's (2006) and Priscott's (2010)
/// work aswell. Some of the functions are using references from web. They can be found within the function
/// itself.
//------------------------------------------------------------------------------------------------------------

//ctor
SpatialHashing::SpatialHashing(const int _partCount, const float _h)
{
  //large prime numbers, retrieved from Teschner' paper (2003)
  m_p1 = 73856093;
  m_p2 = 19349663;
  m_p3 = 83492791;
  m_cellSize = _h;
  m_n = nextPrime(2*_partCount);//according to Ihmsen' paper
}

//dtor
SpatialHashing::~SpatialHashing()
{
  clearHashmap();
}

//find neighbors function. First iterating through the bounding box then passes to the same hash index key search
void SpatialHashing::findNeighbors(Particle * & io_p)
{
  //DIFFERENT HASH KEY BUT INSIDE THE BOUNDING BOX SECTION
  //intersection test section from Teschner' paper
  ngl::Vec3 bbMin, bbMax;
  ngl::Vec3 _hVec(m_cellSize, m_cellSize, m_cellSize);
  bbMin = io_p->getCurrPos() - _hVec;
  bbMax = io_p->getCurrPos() + _hVec;

  //iterating through the three dimensions, this idea is retrieved from Priscott' (2010) work
  float iteration = 0.85;
  for(float i=bbMin.m_x; i<=bbMax.m_x; i+=iteration)
  {
    for(float j=bbMin.m_y; j<=bbMax.m_y; j+=iteration)
    {
      for(float k=bbMin.m_z; k<=bbMax.m_z; k+=iteration)
      {
        //discretize the temporary position and get the hash index key and search the map using this key
        ngl::Vec3 temp(i, j, k);
        int tempKey = getKey(discretize(temp));
        searchMap(io_p, tempKey);
      }
    }
  }

  //SAME HASH KEY SECTION
  //get the key value of the current particle according to its position
  int key = getKey(discretize(io_p->getCurrPos()));
  searchMap(io_p, key);
}

void SpatialHashing::searchMap(Particle * & io_p, const int io_key)
{
  //using equal range method given in this link. username: mcdave
  //http://stackoverflow.com/questions/1898201/c-find-multiple-keys-from-a-stdmultimap

  //create a pair to check neighbors
  std::pair<hashmap::iterator, hashmap::iterator> neighborCheck;
  //assign the key to search
  neighborCheck = m_map.equal_range(io_key);

  //clear the little hash map for check list function
  m_idMap.clear();
  //iterate through the options
  for(hashmap::iterator itr = neighborCheck.first; itr != neighborCheck.second; ++itr)
  {
    Particle *neighbor = (*itr).second;

    //get the hash index key of neighbor candidate to check if its in the neighbor list
    int _IDkey = mod(neighbor->getID()*m_p1, m_n);

    //if neighbor doesnt already exists in the list
    if(!checkList(_IDkey))
    {
      ngl::Vec3 dist = io_p->getCurrPos() - neighbor->getCurrPos();
      //if temp neighbor is within the range
      if(dist.length() <= m_cellSize)
      {
        //add it to the neighbor list
        io_p->pushToNeighborList(neighbor);
        //also add it to checklist hash map
        m_idMap.insert(std::pair<int,int>(_IDkey, neighbor->getID()));
      }//if distance
    }//if checklist

  }//for hashmap iterator
}

bool SpatialHashing::checkList(const int io_key)
{
  //checking whether the given particle is in the neighborlist or not
  //a similar function is also used in octree implementation too(this one is using hash map
  //instead of list)
  if(m_idMap.find(io_key) != m_idMap.end())
  {
    //if input id is in the std::vector return true
    return true;
  }
  //if its not in the list, then false
  return false;
}

void SpatialHashing::fillHashmap(std::vector<Particle *> io_particles)
{
  int key = 0;
  BOOST_FOREACH(Particle *_p, io_particles)
  {
    //clear the neighbor list for every particle
    _p->clearNeighborList();
    //get the hash index key of particle
    key = getKey(discretize(_p->getCurrPos()));
    //and insert the particle to the hash map with corresponding index key
    m_map.insert(std::pair<int, Particle *>(key, _p));
  }
}

int SpatialHashing::getKey(const ngl::Vec3 io_pos)
{
  //and calculate the hash key using the hash function declared in Teschner' paper 2003
  int key = mod((int)(io_pos.m_x * m_p1) ^ (int)(io_pos.m_y * m_p2) ^ (int)(io_pos.m_z * m_p3), m_n);
  return key;
}

int SpatialHashing::mod(const int io_a, const int io_b)
{
  //taking the modulus of given input integers. used the following reference because of the negative valued
  //results
  //http://stackoverflow.com/questions/1082917/mod-of-negative-number-is-melting-my-brain
  //username: ShreevatsaR
  int r = io_a % io_b;
  return r < 0 ? r + io_b : r;
}

ngl::Vec3 SpatialHashing::discretize(const ngl::Vec3 io_pos)
{
  //discretizing the input position with respect to the cell size
  ngl::Vec3 temp;
  temp.m_x = floor(io_pos.m_x/m_cellSize);
  temp.m_y = floor(io_pos.m_y/m_cellSize);
  temp.m_z = floor(io_pos.m_z/m_cellSize);
  return temp;
}

int SpatialHashing::nextPrime(int io_num)
{
  //in order to obtain the hash table size, we use this function
  //according to Ihmsen' paper, the hash table size should be atleast twice of the particle count
  //in order to obtain unique results with hash function, we want the table size to be a prime number

  //following link is the reference link where we got this function:
  //http://stackoverflow.com/questions/30052316/find-next-prime-number-algorithm
  while(!isPrime(++io_num))
  {

  }
  return io_num;
}

bool SpatialHashing::isPrime(const int io_num)
{
  //reference link to where we got this function:
  //http://stackoverflow.com/questions/30052316/find-next-prime-number-algorithm
  if(io_num == 2 || io_num == 3)
  {
    return true;
  }
  if(io_num % 2 == 0 || io_num % 3 == 0)
  {
    return false;
  }
  int div = 6;
  while(div*div - 2*div + 1 <= io_num)
  {
    if(io_num % (div - 1) == 0)
    {
      return false;
    }
    if(io_num % (div + 1) == 0)
    {
      return false;
    }
    div += 6;
  }
  return true;
}
