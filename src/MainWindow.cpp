#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <QApplication>
#include <QFileDialog>
#include <QDir>
//------------------------------------------------------------------------------------------------------------
/// @file MainWindow.cpp
/// @author Burak Ertekin
/// @version 1.0
/// @date 18/08/15
/// @brief MainWindow class source. Connecting UI elements the the system
//------------------------------------------------------------------------------------------------------------

MainWindow::MainWindow(QWidget *parent) :QMainWindow(parent),  m_ui(new Ui::MainWindow)
{
    m_ui->setupUi(this);
    // create an openGL format and pass to the new GLWidget
    QGLFormat format;
    format.setVersion(3,2);
    format.setProfile(QGLFormat::CoreProfile);

    m_gl=new GLWindow(format, this);

    m_fluidType = true;

    m_ui->simBoxPath->setText("/models/boxTenMeter.obj");
    m_ui->fluidObjPath->setText("/models/4ksphere.obj");

    //screen element
    m_ui->screen->addWidget(m_gl,0,0,12,1);
    //run/pause button
    connect(m_ui->runButton, SIGNAL(clicked(bool)), this, SLOT(run(bool)));
    connect(m_ui->resetButton, SIGNAL(clicked()), this, SLOT(reset()));

    //input simulation parameters
    connect(m_ui->simBoxPath, SIGNAL(textChanged()), this, SLOT(getFilenameBox()));
    connect(m_ui->fluidObjPath, SIGNAL(textChanged()), this, SLOT(getFilenameFluid()));
    connect(m_ui->fileBrowse, SIGNAL(clicked()), this, SLOT(getPathBox()));
    connect(m_ui->fileBrowse_2, SIGNAL(clicked()), this, SLOT(getPathFluid()));
    //newtonian elements
    connect(m_ui->newGasConstantBox, SIGNAL(valueChanged(double)), m_gl, SLOT(uisetGasConstant(double)));
    connect(m_ui->newVolumeBox, SIGNAL(valueChanged(double)), m_gl, SLOT(uiSetVolume(double)));
    connect(m_ui->newSmoothBox, SIGNAL(valueChanged(double)), m_gl, SLOT(uiSetSmoothingLength(double)));
    connect(m_ui->newTensionBox, SIGNAL(valueChanged(double)), m_gl, SLOT(uisetTension(double)));
    connect(m_ui->newViscoBox, SIGNAL(valueChanged(double)), m_gl, SLOT(uisetViscosityCoeff(double)));
    connect(m_ui->newThresholdBox, SIGNAL(valueChanged(double)), m_gl, SLOT(uisetSurfaceThreshold(double)));
    connect(m_ui->newDensityBox, SIGNAL(valueChanged(double)), m_gl, SLOT(uisetRestDensity(double)));
    //non-newtonian elements
    connect(m_ui->nonNewElasticityBox, SIGNAL(valueChanged(double)), m_gl, SLOT(uisetElasticity(double)));
    connect(m_ui->nonNewGastConstantBox, SIGNAL(valueChanged(double)), m_gl, SLOT(uisetGasConstant(double)));
    connect(m_ui->nonNewRelaxationBox, SIGNAL(valueChanged(double)), m_gl, SLOT(uisetRelaxation(double)));
    connect(m_ui->nonNewSmoothBox, SIGNAL(valueChanged(double)), m_gl, SLOT(uiSetSmoothingLength(double)));
    connect(m_ui->nonNewViscoBox, SIGNAL(valueChanged(double)), m_gl, SLOT(uisetViscosityCoeff(double)));
    connect(m_ui->nonNewVolumeBox, SIGNAL(valueChanged(double)), m_gl, SLOT(uiSetVolume(double)));
    connect(m_ui->nonNewDensityBox, SIGNAL(valueChanged(double)), m_gl, SLOT(uisetRestDensity(double)));
    //radio button selection
    connect(m_ui->newtonianRadio, SIGNAL(toggled(bool)), this, SLOT(newtonianSelected(bool)));
    connect(m_ui->nonNewtonianRadio, SIGNAL(toggled(bool)), this, SLOT(nonNewtonianSelected(bool)));
    //opengl draw checkbox and collision condition
    connect(m_ui->openglCheckBox, SIGNAL(stateChanged(int)), m_gl, SLOT(checkDraw(int)));
    connect(m_ui->collisionCheckBox, SIGNAL(stateChanged(int)), m_gl, SLOT(collisionCond(int)));

}

//dtor
MainWindow::~MainWindow()
{
    delete m_ui;
}

//run or stop the simulation according to the boolean value
void MainWindow::run(bool io_simRun)
{
  if(io_simRun)
  {
    m_gl->runSimulation();
  }
  else
  {
    m_gl->stopSimulation();
  }
}

//reset the simulation
void MainWindow::reset()
{
  if(m_fluidType)
  {
    m_gl->resetSimulation(m_ui->newVolumeBox->value(), m_ui->newSmoothBox->value(), m_ui->newViscoBox->value(), m_ui->newDensityBox->value(),
                          m_ui->newGasConstantBox->value(), m_ui->newTensionBox->value(), m_ui->newThresholdBox->value(),
                          m_ui->nonNewRelaxationBox->value(), m_ui->nonNewElasticityBox->value());
  }
  else
  {
    m_gl->resetSimulation(m_ui->nonNewVolumeBox->value(), m_ui->nonNewSmoothBox->value(), m_ui->nonNewViscoBox->value(),
                          m_ui->nonNewDensityBox->value(), m_ui->nonNewGastConstantBox->value(), m_ui->newTensionBox->value(),
                          m_ui->newThresholdBox->value(), m_ui->nonNewRelaxationBox->value(), m_ui->nonNewElasticityBox->value());
  }
}

//get the file path value of simulation box
void MainWindow::getFilenameBox()
{
  QString qtext = m_ui->simBoxPath->toPlainText();
  std::string text = qtext.toUtf8().constData();
  m_gl->setBoxFilePath(text);
}

//get the file path value of fluid object
void MainWindow::getFilenameFluid()
{
  QString qtext = m_ui->fluidObjPath->toPlainText();
  std::string text = qtext.toUtf8().constData();
  m_gl->setFluidFilePath(text);
}

//qfiledialog function for simulation box
void MainWindow::getPathBox()
{
  QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),"/path/to/file/",tr(".Obj Files (*.obj)"));
  m_ui->simBoxPath->setText(fileName);
}

//qfiledialog function for fluid object
void MainWindow::getPathFluid()
{
  QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),"/path/to/file/",tr(".Obj Files (*.obj)"));
  m_ui->fluidObjPath->setText(fileName);
}
