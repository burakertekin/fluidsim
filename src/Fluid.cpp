#include "Fluid.h"
//------------------------------------------------------------------------------------------------------------
/// @file Fluid.cpp
/// @author Burak Ertekin
/// @version 1.0
/// @date 17/08/15
/// @brief Fluid class source. Main solver of this implementation. Initialisation, updating and reseting
/// functions. Maintains all the connections with UI and other classes to create the simulation. There are
/// two main algorithms: update() and nonNewtonianUpdate() for Newtonian and Non-Newtonian simulations.
//------------------------------------------------------------------------------------------------------------

#define SQR(x)  (x)*(x)

Fluid::Fluid()
{
  //default boolean values for checkbox and data writing features
  m_collisionCondition = false;
  m_writeCheck = true;
  //default .obj file paths
  m_boxObjectPath = "models/boxTenMeter.obj";
  m_fluidObjectPath ="models/4ksphere.obj";

  //heigh of the octree
  m_height = 3;
  //variable to track the id of the particle.
  m_idTrack = 0;

  //smoothing length for SPH
  m_h = 0.3;
  //volume of the fluid object
  m_volume = 21;
  //viscosity coefficient
  m_viscCoeff = 300;
  //surface tension coefficient
  m_tensionCoeff = 300;
  //surface normal threshold
  m_surfaceThreshold = 4.0;
  //gas constant for scalar pressure value
  m_gasConstant = 10;
  //rest density value of the value for all particles
  m_restDensity = 998.2;
  //relaxation time of the material
  m_relaxation = 0.1;
  //elasticity coefficient
  m_elasticity = 1000;
}

Fluid::~Fluid()
{
  //clear all the particles
  clearParticles();
  //clear the walls from the scene
  m_myScene.clearWalls();
  //clear the obstacles
  m_sphObstacles.clear();
}

void Fluid::reset()
{
  //clear the partciles
  clearParticles();
  //clear the walls
  m_myScene.clearWalls();
  //delete the hash map
  delete m_hashMap;
}

void Fluid::init()
{
  //set the iteration count to 0
  m_itr = 0;

  //oath initialise the .obj objects
  m_simulationBox = new ngl::Obj(m_boxObjectPath);
  m_fluidObject = new ngl::Obj(m_fluidObjectPath);

  //setting the simulation box dimension and coordinates according to
  //the bounding box from the input simulation box
  m_myScene.setSimulationBox(m_simulationBox);

  //setting the scene features to collision class so the collision detection
  m_collision.setScene(m_myScene);

  //read in the .obj file vertex list to create the fluid object as particles
  BOOST_FOREACH(ngl::Vec3 _vert, m_fluidObject->getVertexList())
  {
    Particle *b = new Particle;
    //set the position of the vertex
    b->setPos(_vert);
    //set the id
    b->setID(m_idTrack);
    //set the mass, this function is used by Kelager (2006)
    b->setMass(m_restDensity*(m_volume/m_fluidObject->getNumVerts()));
    //push the particle to the list
    m_particles.push_back(b);
    ++m_idTrack;
  }

  //create a new hashmap according to the particle count and smoothing length
  m_hashMap = new SpatialHashing(m_particles.size(), m_h);

  //sphere radius for obstacles
  m_sphereRad = 1.0f;
  //sphere collision scenario - 1 (3 spheres on bottom boundary)
  //obstacle sphere creation - basic collision detection
  for(int i = 0; i<3; ++i)
  {
    ngl::Vec3 tempPos(1+i, -4, ((i+5)*2-12));
    m_sphObstacles.push_back(tempPos);
  }

  /*
  //sphere collision scenario - 2 (1 sphere in the middle of the box)
  ngl::Vec3 tempPos(0, -2, 0);
  m_sphObstacles.push_back(tempPos);*/
}

void Fluid::calculateDensity()
{
  BOOST_FOREACH(Particle *_p, m_particles)
  {
    //fill the neighbor std::vector for each particle
    //spatial hashing algorithm
    m_hashMap->findNeighbors(_p);

    //set the density to zero since it will be incremented at each neighbor
    _p->setDensity(0);

    BOOST_FOREACH(Particle *_n, _p->getNeighborList())
    {
      ngl::Vec3 _r = _p->getCurrPos() - _n->getCurrPos();
      Kernel kernel(_r, m_h);
      //spline default kernel as defined in mao & yang' (2003) paper
      //_p->addDensity(kernel.spline() * _n->getMass());
      //poly6 default kernel as defined in muller' (2003) paper
      _p->addDensity(kernel.poly() * _n->getMass());
    }
    //calculate scalar pressure values for each particle
    _p->setScalarPres(0);
    _p->setScalarPres(m_gasConstant*(_p->getDensity() - m_restDensity));
    //if(_p->getScaPressure() < 0)
    //{
      //_p->setScalarPres(0);
    //}
  }
}

void Fluid::update()
{
  //clear the hash map
  m_hashMap->clearHashmap();

  //fill the hash map
  m_hashMap->fillHashmap(m_particles);

  //calculate densities for each particle
  calculateDensity();

  BOOST_FOREACH(Particle *_p, m_particles)
  {
    //clear force object
    m_particleForces.clearForces();

    //local variables to increase readibility
    ngl::Vec3 pressure, viscosity, surfaceNormal;
    float surfaceTension = 0;
    BOOST_FOREACH(Particle *_n, _p->getNeighborList())
    {
      //if the neighbor and the current particle is not the same
      if(_p->getID()!= _n->getID())
      {
        //get pressure and velocity terms of the particle according to its neighbors
        //equation 10 and 14 from Muller' (2003) paper
        pressure = m_particleForces.getPressure(_p, _n, m_h);
        viscosity = m_particleForces.getViscosity(_p, _n, m_h);
      }

      //to get surface tension term
      surfaceNormal = m_particleForces.getSurfaceNormal(_p, _n, m_h);
      surfaceTension = m_particleForces.getSurfaceTension(_p, _n, m_h);

    }

    ngl::Vec3 viscForce, externalForce, surfaceForce;
    viscForce = m_viscCoeff*viscosity;
    externalForce = m_particleForces.addGravity();

    //if the particle surface normal passes the threshold value, calculate the new surface tension
    if(surfaceNormal.length() > m_surfaceThreshold)
    {
      //equation 19 from Muller' (2003) paper
      surfaceNormal.normalize();
      surfaceForce = -m_tensionCoeff * surfaceTension * surfaceNormal;
    }

    //divide with the density value before adding to the net force
    viscForce = viscForce / _p->getDensity();
    surfaceForce = surfaceForce / _p->getDensity();

    ngl::Vec3 acceleration;
    //new acceleration

    //add them all to retrieve the net force
    acceleration = -pressure + viscForce + externalForce + surfaceForce;

    //update the acceleration of the particle
    _p->updateAcc(acceleration);
  }

  //initialise the output file to write, since it's writing a single file each iteration
  m_output.init(m_itr);

  //integrate to obtain new positions for the particles
  BOOST_FOREACH(Particle *_p, m_particles)
  {
    //different integration schemes
    //m_integrate.leapFrog(_p);
    //m_integrate.explicitEuler(_p);
    m_integrate.semiImplicitEuler(_p);

    //velocity correction algorithm
    //m_integrate.velocityCorrectionXSPH(_p, m_h, 0.25);

    //check for collisions with both the boundaries and the obstacles
    m_collision.checkWalls(_p);
    if(m_collisionCondition)
    {
      m_collision.checkSphere(_p, m_sphObstacles, m_sphereRad);
    }

    //write out the data
    m_output.writeStep(_p);

  }
  //close the output file
  m_output.finish();

  //increase iteration
  ++m_itr;
}

void Fluid::updateNonNewtonian()
{
  //clear the hash map
  m_hashMap->clearHashmap();

  //fill the hash map
  m_hashMap->fillHashmap(m_particles);

  //calculate densities for each particle
  calculateDensity();

  //calculate stress tensor for every particle
  BOOST_FOREACH(Particle *_p, m_particles)
  {
    m_particleForces.clearForces();
    m_particleForces.calculateStressTensor(_p, m_h, m_elasticity, m_relaxation, m_integrate.getTimestep());
  }
  BOOST_FOREACH(Particle *_p, m_particles)
  {
    m_particleForces.clearForces();

    ngl::Vec3 pressure, viscosity, stress;
    BOOST_FOREACH(Particle *_n, _p->getNeighborList())
    {
      if(_p->getID()!= _n->getID())
      {
        pressure = m_particleForces.getPressure(_p, _n, m_h);
        viscosity = m_particleForces.getViscosity(_p, _n, m_h);
      }
      //get the stress term, equation 3rd term from equation 1 - mao & yang' (2003) paper
      stress = m_particleForces.getStress(_p, _n, m_h);
    }

    ngl::Vec3 viscForce, externalForce;
    viscForce = m_viscCoeff*viscosity / _p->getDensity();
    externalForce = m_particleForces.addGravity();

    ngl::Vec3 acceleration;
    //new acceleration

    acceleration = -pressure + viscForce + externalForce + stress;

    _p->updateAcc(acceleration);
  }

  m_output.init(m_itr);
  //integrate to obtain new positions for the particles
  BOOST_FOREACH(Particle *_p, m_particles)
  {
    //m_integrate.leapFrog(_p);
    //m_integrate.explicitEuler(_p);
    m_integrate.semiImplicitEuler(_p);

    //m_integrate.velocityCorrectionXSPH(_p, m_h, 0.55);

    //check for collisions
    m_collision.checkWalls(_p);
    if(m_collisionCondition)
    {
      m_collision.checkSphere(_p, m_sphObstacles, m_sphereRad);
    }

    //write out the data
    m_output.writeStep(_p);

  }
  m_output.finish();
  //increase iteration
  ++m_itr;
}

void Fluid::clearParticles()
{
  //clear the particle list
  BOOST_FOREACH(Particle *_p, m_particles)
  {
    delete _p;
  }
  m_particles.clear();
}
