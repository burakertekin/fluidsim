#include "Kernel.h"
#include "ngl/Util.h"
#include <iostream>
//------------------------------------------------------------------------------------------------------------
/// @file Kernel.cpp
/// @author Burak Ertekin
/// @version 1.0
/// @date 18/08/15
/// @brief Kernel class source. Calculating different kernel functions. Cubic spline, poly6, spiky and viscosity
/// kernel functions can be found.
//------------------------------------------------------------------------------------------------------------

//helpful math function shorcuts for kernels
#define SQR(x)		(x*x)
#define CUBE(x)		(x*x*x)
#define POW4(x)   (SQR(x)*SQR(x))
#define POW5(x)   (CUBE(x)*SQR(x))
#define POW6(x)		(CUBE(x)*CUBE(x))
#define POW9(x)   (POW6(x)*CUBE(x))

//ctor
Kernel::Kernel(const ngl::Vec3 _r, const float _h)
{
  m_r = _r;
  m_h = _h;
}

//default cubic spline kernel - mao & yang
float Kernel::spline()
{
  //mao and yang paper
  float _q = 2*(m_r.length() / m_h);

  if(0<=_q && _q<=1)
  {
    return (1/(ngl::PI * CUBE(m_h))) * (1 - 1.5*SQR(_q) + 0.75*CUBE(_q));
  }
  else if(1<=_q && _q<=2)
  {
    return (1/(ngl::PI * CUBE(m_h))) * (0.25*CUBE((2 - _q)));
  }
  else
  {
    return 0.0;
  }

}

//gradient cubic spline kernel - mao & yang
ngl::Vec3 Kernel::gradientSpline()
{
  //for stress tensor from mao and yangs paper
  float _q = (m_r.length() / m_h);

  if(0<=_q && _q<=1)
  {
    return (9.0/(4.0 * ngl::PI * POW5(m_h))) * (_q - 1.33) * m_r;
  }
  else if(1<=_q && _q<=2)
  {
    return (9.0/(4.0 * ngl::PI * CUBE(m_h))) * -0.33 * SQR((2.0 - _q)) * (1.0/_q) * m_r;
  }
  else
  {
    ngl::Vec3 temp;
    temp.null();
    return temp;
  }
}

//gradient spiky kernel - desburn
ngl::Vec3 Kernel::gradientSpiky()
{
  //spkiy kernel from mao and yangs paper
  //gradient of the kernel
  float _R = m_r.length();

  if(0<=_R && _R<=m_h)
  {
    m_r.normalize();
    return (-45.0f / (ngl::PI * POW6(m_h))) * SQR((m_h - _R)) * m_r;
  }
  else
  {
    ngl::Vec3 temp;
    temp.null();
    return temp;
  }
}

//default poly6 kernel - muller
float Kernel::poly()
{
  //poly6 kernel from muller' paper 2003
  float _r = m_r.length();
  if(0<=_r && _r<=m_h)
  {
    return (315.0/(64.0*ngl::PI*POW9(m_h))) * CUBE((SQR(m_h) - SQR(_r)));
  }
  else
  {
    return 0;
  }
}

//gradient poly6 kernel - muller
ngl::Vec3 Kernel::gradientPoly()
{
  //poly6 kernel gradient version. poly6 kernel from muller' paper 2003
  float _r = m_r.length();
  if(0<=_r && _r<=m_h)
  {
    return (-945.0/(32.0*ngl::PI*POW9(m_h))) * SQR((SQR(m_h) - SQR(_r))) * m_r;
  }
  else
  {
    ngl::Vec3 temp;
    temp.null();
    return temp;
  }
}

//laplacian poly6 kernel - muller
float Kernel::laplacianPoly()
{
  //poly6 kernel laplacian version. poly6 kernel from muller' paper 2003
  float _r = m_r.length();
  if(0<=_r && _r<=m_h)
  {
    return (-945.0/(32.0*ngl::PI*POW9(m_h))) * (SQR(m_h) - SQR(_r)) * (3*SQR(m_h) - 7*SQR(_r));
  }
  else
  {
    return 0.0;
  }
}

//viscosity kernel - muller
float Kernel::viscoLaplacian()
{
  //laplacian viscosity kernel from muller' paper 2003
  float _r = m_r.length();
  if(0<=_r && _r<=m_h)
  {
    return (45.0/(ngl::PI*POW6(m_h))) * (m_h - _r);
  }
  else
  {
    return 0.0;
  }
}
