#include "GLWindow.h"
#include <iostream>
#include <ngl/NGLInit.h>
#include <ngl/VAOPrimitives.h>
#include <ngl/ShaderLib.h>
#include <ngl/Util.h>
#include <iostream>
#include <ngl/NGLStream.h>
//------------------------------------------------------------------------------------------------------------
/// @file GLWindow.cpp
/// @author Jon Macey
/// @version 1.0
/// @date 18/08/15
/// @brief All the visualization of our simulation is lied in this file. GLWindow class retrieved from one of
/// NGL Demos and adjusted for this project
//------------------------------------------------------------------------------------------------------------

/// @brief these const parameters are defined for mouse events
const static float INCREMENT=0.05;
const static float ZOOM=1;

GLWindow::GLWindow(const QGLFormat _format, QWidget *_parent ) : QGLWidget( _format, _parent )
{
  // re-size the widget to that of the parent (in this case the GLFrame passed in on construction)
  m_rotate=false;
  // mouse rotation values set to 0
  m_spinXFace=0;
  m_spinYFace=0;
  m_timer_value = 1;
  //initially simulation doesn't run
  stopSimulation();
  //initially simulation draws the elements
  m_checkDraw = true;
  //initially simulation is a Newtonian one
  m_fluidType = true;
}

//draw spheres for collision obstacles
void GLWindow::drawSpheres(ngl::Vec3 io_obstacle)
{
  ngl::ShaderLib *shader=ngl::ShaderLib::instance();
  (*shader)["nglDiffuseShader"]->use();
  shader->setShaderParam4f("Colour", 0.6f, 0.5f, 0.0f, 0.0f);
  m_transform.reset();
  m_sphere = ngl::VAOPrimitives::instance();
  m_sphere->createSphere("sphere", m_myFluid.getObstacleRadius(), 30);
  m_transform.setPosition(io_obstacle.m_x, io_obstacle.m_y, io_obstacle.m_z);

  ngl::Mat4 MVP= m_transform.getMatrix()
                 *m_mouseGlobalTX* m_cam->getVPMatrix();
  shader->setShaderParamFromMat4("MVP",MVP);
  m_sphere->draw("sphere");
}

//draw particles to represent fluids
void GLWindow::drawParticles(ngl::Vec3 io_pos, float io_radius, ngl::Colour io_colour)
{
  ngl::Mat3 normalMatrix;

  ngl::ShaderLib *shader=ngl::ShaderLib::instance();
  (*shader)["nglDiffuseShader"]->use();
  shader->setShaderParam4f("Colour",io_colour.m_r,io_colour.m_g,io_colour.m_b,io_colour.m_a);
  ngl::Transformation t;
  t.setPosition(io_pos);
  t.setScale(io_radius,io_radius,io_radius);
  ngl::Mat4 MVP=t.getMatrix() * m_mouseGlobalTX * m_cam->getVPMatrix();
  normalMatrix=t.getMatrix()* m_mouseGlobalTX*m_cam->getViewMatrix();
  normalMatrix.inverse();
  shader->setRegisteredUniform("MVP",MVP);
  shader->setRegisteredUniform("normalMatrix",normalMatrix);

  // get an instance of the VBO primitives for drawing
  ngl::VAOPrimitives::instance()->draw("sphere");

}

//draw all the elements found in the scene
void GLWindow::drawScene()
{
  //draw the particles
  BOOST_FOREACH(Particle *p, m_myFluid.getParticles())
  {
    drawParticles(p->getCurrPos(), p->getRadius(), p->getColour());
  }
  // draw the walls
  ngl::VAOPrimitives *prim=ngl::VAOPrimitives::instance();
  ngl::ShaderLib *shader=ngl::ShaderLib::instance();
  (*shader)["nglColourShader"]->use();
  BOOST_FOREACH(Wall *w, m_myScene.getWalls())
  {
    if(w->draw_flag)
    {
      m_transform.reset();
      {
        shader->setShaderParam4f("Colour", 0.0f, 0.0f, 0.0f, 0.0f);
        m_transform.setPosition(w->centre);
        m_transform.setScale(w->size, w->size, w->size);
        m_transform.setRotation(getRotationFromY(ngl::Vec3(w->a,w->b,w->c)));
        ngl::Mat4 MVP= m_transform.getMatrix()
                       *m_mouseGlobalTX* m_cam->getVPMatrix();
        shader->setShaderParamFromMat4("MVP",MVP);
        prim->draw("wall");
      }
    }
  }

  if(m_myFluid.getCollisionCondition())
  {
    BOOST_FOREACH(ngl::Vec3 sph, m_myFluid.getObstacles())
    {
      drawSpheres(sph);
    }
  }
}

ngl::Vec3 GLWindow::getRotationFromY(ngl::Vec3 _vec) const
{
    ngl::Vec3 rot;
    rot.m_z = 0.0;
    if(fabs(_vec.m_y)< 0.0001)
    {
        if (_vec.m_z>= 0.0)
            rot.m_x = -90;
        else
            rot.m_x = 90;
    }
    else
        rot.m_x = atan(_vec.m_z/_vec.m_y);
    if(fabs(_vec.m_y) + fabs(_vec.m_z) < 0.0001)
    {
        if(_vec.m_x > 0)
            rot.m_y = -90;
        else
            rot.m_y = 90;
    }
    else
        rot.m_z = atan(_vec.m_x/sqrt(_vec.m_y*_vec.m_y + _vec.m_z*_vec.m_z));

    return rot;
}

void GLWindow::initScene()
{
  //initialise both the fluid and the scene
  m_myFluid.init();

  m_myScene = m_myFluid.getScene();
  m_myScene.init();
}

void GLWindow::initializeGL()
{
  // we need to initialise the NGL lib which will load all of the OpenGL functions, this must
  // be done once we have a valid GL context but before we call any GL commands. If we dont do
  // this everything will crash
  ngl::NGLInit::instance();
  glClearColor(0.4f, 0.4f, 0.4f, 1.0f);			   // Grey Background
  // enable depth testing for drawing
  glEnable(GL_DEPTH_TEST);
  // enable multisampling for smoother drawing
  glEnable(GL_MULTISAMPLE);

  // Now we will create a basic Camera from the graphics library
  // This is a static camera so it only needs to be set once
  // First create Values for the camera position
  ngl::Vec3 from(0,0,10);
  ngl::Vec3 to(0,0,0);
  ngl::Vec3 up(0,1,0);
  m_cam= new ngl::Camera(from,to,up);
  // set the shape using FOV 45 Aspect Ratio based on Width and Height
  // The final two are near and far clipping planes of 0.5 and 10
  m_cam->setShape(45,(float)720.0/576.0,0.5,150);
  // now to load the shader and set the values
  // grab an instance of shader manager
  // now to load the shader and set the values
  // grab an instance of shader manager
  ngl::ShaderLib *shader=ngl::ShaderLib::instance();
  (*shader)["nglDiffuseShader"]->use();
  shader->setShaderParam4f("Colour",1,1,0,1);
  shader->setShaderParam3f("lightPos",1,1,1);
  shader->setShaderParam4f("lightDiffuse",1,1,1,1);

  glEnable(GL_DEPTH_TEST); // for removal of hidden surfaces

  ngl::VAOPrimitives *prim=ngl::VAOPrimitives::instance();
  prim->createSphere("sphere",1.0,20);
  prim->createLineGrid("wall", 1, 1, 5);

  m_text= new ngl::Text(QFont("Helvetica", 14));

  m_text->setScreenSize(600,600);

  m_currentTime = m_currentTime.currentTime();
  initScene();
  glViewport(0,0,width(),height());

}

void GLWindow::paintGL()
{
  // clear the screen and depth buffer
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  QTime newTime = m_currentTime.currentTime();
  int msecpassed = m_currentTime.msecsTo(newTime);
  m_currentTime = newTime;

  // grab an instance of the shader manager
  ngl::ShaderLib *shader=ngl::ShaderLib::instance();
  // clear the screen and depth buffer
  shader->use("nglDiffuseShader");

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  ngl::Mat4 rotX;
  ngl::Mat4 rotY;
  // create the rotation matrices
  rotX.rotateX(m_spinXFace);
  rotY.rotateY(m_spinYFace);
  // multiply the rotations
  m_mouseGlobalTX=rotY*rotX;
  // add the translations
  m_mouseGlobalTX.m_m[3][0] = m_modelPos.m_x;
  m_mouseGlobalTX.m_m[3][1] = m_modelPos.m_y;
  m_mouseGlobalTX.m_m[3][2] = m_modelPos.m_z;

  //draw the text
  QString text;
  text.sprintf("framerate is %d", (int)(1000.0/msecpassed));
  m_text->renderText(5,5, text);
  text.sprintf("iteration count is %d", (m_myFluid.getItr()));
  m_text->renderText(5,25,text);

  if(m_checkDraw)
  {
    //draw the scene
    drawScene();
  }
}

void GLWindow::mouseMoveEvent (QMouseEvent * _event)
{
    // note the method buttons() is the button state when event was called
    // this is different from button() which is used to check which button was
    // pressed when the mousePress/Release event is generated
    if(m_rotate && _event->buttons() == Qt::LeftButton)
    {
      int diffx=_event->x()-m_origX;
      int diffy=_event->y()-m_origY;
      m_spinXFace += (float) 0.5f * diffy;
      m_spinYFace += (float) 0.5f * diffx;
      m_origX = _event->x();
      m_origY = _event->y();
    }
    // right mouse translate code
    else if(m_translate && _event->buttons() == Qt::RightButton)
    {
      int diffX = (int)(_event->x() - m_origXPos);
      int diffY = (int)(_event->y() - m_origYPos);
      m_origXPos=_event->x();
      m_origYPos=_event->y();
      m_modelPos.m_x += INCREMENT * diffX;
      m_modelPos.m_y -= INCREMENT * diffY;
     }
    updateGL();
}

void GLWindow::mousePressEvent (QMouseEvent * _event )
{
  // this method is called when the mouse button is pressed in this case we
  // store the value where the maouse was clicked (x,y) and set the Rotate flag to true
  if(_event->button() == Qt::LeftButton)
  {
    m_origX = _event->x();
    m_origY = _event->y();
    m_rotate =true;
  }
  // right mouse translate mode
  else if(_event->button() == Qt::RightButton)
  {
    m_origXPos = _event->x();
    m_origYPos = _event->y();
    m_translate=true;
  }
}

void GLWindow::mouseReleaseEvent (QMouseEvent * _event )
{
  // this event is called when the mouse button is released
  // we then set Rotate to false
  if (_event->button() == Qt::LeftButton)
  {
    m_rotate=false;
  }
        // right mouse translate mode
  if (_event->button() == Qt::RightButton)
  {
    m_translate=false;
  }
}


GLWindow::~GLWindow()
{
  ngl::NGLInit *init = ngl::NGLInit::instance();
  //init->NGLQuit();
  m_myScene.clearWalls();
}

void GLWindow::wheelEvent(QWheelEvent *_event)
{
	// check the diff of the wheel position (0 means no change)
		if(_event->delta() > 0)
		{
			m_modelPos.m_z+=ZOOM;
		}
		else if(_event->delta() <0 )
		{
			m_modelPos.m_z-=ZOOM;
		}
		updateGL();
}

void GLWindow::keyPressEvent(QKeyEvent *_event)
{
  switch(_event->key())
  {
    //case Qt::Key_Escape : QApplication::exit(EXIT_FAILURE); break;
  default: break;
  }
  updateGL();
}

void GLWindow::timerEvent(QTimerEvent *_event)
{
  if(m_myFluid.getItr() == 3000)
  {
    stopSimulation();
  }
  else
  {
  //update functions
  if(m_fluidType)
  {
    m_myFluid.update();
  }
  else
  {
    m_myFluid.updateNonNewtonian();
  }
  updateGL();
  }
}

//reset the simultaion, set the parameters from UI to the system
void GLWindow::resetSimulation(const double io_vol, const double io_h, const double io_visc, const double io_density, const double io_gasConstant, const double io_tension,
                               const double io_threshold, const double io_relaxation, const double io_elasticity)
{
  m_myFluid.setVolume(io_vol);
  m_myFluid.setGasConstant(io_gasConstant);
  m_myFluid.setRestDensity(io_density);
  m_myFluid.setSmoothingLength(io_h);
  m_myFluid.setViscosityCoeff(io_visc);
  m_myFluid.setTension(io_tension);
  m_myFluid.setSurfaceThreshold(io_threshold);
  m_myFluid.setElasticity(io_elasticity);
  m_myFluid.setRelaxation(io_relaxation);

  m_myScene.clearWalls();
  m_myFluid.reset();
  initScene();
}

//radio button function
void GLWindow::radioSelect(const int io_radioIndex)
{
  if(io_radioIndex == 0)
  {
    //set the fluid to newtonian
    m_fluidType = true;
  }
  else
  {
    //set the fluid to non-newtonian
    m_fluidType = false;
  }
}

//drawing checkbox function
void GLWindow::checkDraw(const int io_check)
{
  if(io_check == 0)
  {
    m_checkDraw = false;
  }
  else
  {
    m_checkDraw = true;
  }
}

//collision condition checkbox function
void GLWindow::collisionCond(const int io_check)
{
  if(io_check == 0)
  {
    m_myFluid.setCollisionCondition(false);
  }
  else
  {
    m_myFluid.setCollisionCondition(true);
  }
}
