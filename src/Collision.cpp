#include "Collision.h"
//------------------------------------------------------------------------------------------------------------
/// @file Collision.cpp
/// @author Burak Ertekin
/// @version 1.0
/// @date 17/08/15
/// @brief Collision class source. Simulation box and a simple sphere object collision
/// Sphere collision algorithm adapted from Cloth Simulation written for ASE
/// reference link
/// http://graphics.stanford.edu/~mdfisher/cloth.html
/// Wall collision algorithm retrieved from Jon Macey's OctreeAbstract Demo
/// reference link
/// https://github.com/NCCA/OctreeAbstract
//------------------------------------------------------------------------------------------------------------

Collision::Collision()
{
}

void Collision::setScene(Scene io_scene)
{
  // set the scene according to the input
  m_myScene = io_scene;
  // initialise the scene
  m_myScene.init();
}

void Collision::checkWalls(Particle * & io_p)
{
  // define local variables to be used inside the function
  ngl::Vec3 oldP, oldV, newP, newV, wallNormal;
  float radius;
  float dist;
  // old velocity and position values are assigned to increase the readibility
  oldP = io_p->getCurrPos();
  oldV = (io_p->getLastVelo() + io_p->getCurrVelo())/2.0;
  radius = io_p->getRadius();
  BOOST_FOREACH(Wall *_w, m_myScene.getWalls())
  {
    // if the particle has some velocity, check for collision
    if(oldV.length() > 0)
    {
      wallNormal.m_x = _w->a;
      wallNormal.m_y = _w->b;
      wallNormal.m_z = _w->c;
      dist = oldP.m_x * _w->a + oldP.m_y * _w->b + oldP.m_z * _w->c + _w->d - radius;
      if(dist < 0.0)
      {
        //if penetrate the wall then reflect the particle out and update new
        //position and velocity
        newP = oldP - 3*dist*wallNormal;
        newV = (oldV - 1.0*(oldV.dot(wallNormal)*wallNormal));
        io_p->setPos(newP);
        io_p->setVelo(newV);
      }
    }
  }
}

void Collision::checkSphere(Particle * & io_p, std::vector<ngl::Vec3> io_obstacles, float io_sphereRad)
{
  BOOST_FOREACH(ngl::Vec3 _spherePos, io_obstacles)
  {
    ngl::Vec3 dist = _spherePos - io_p->getCurrPos();
    //collision distance between the particle and sphere
    float collisionDistance = dist.length() - (io_sphereRad + io_p->getRadius());
    dist.normalize();

    ngl::Vec3 newP = dist * collisionDistance;
    ngl::Vec3 oldV, newV, sphereNormal;
    oldV = (io_p->getCurrVelo() + io_p->getLastVelo())/2.0;
    sphereNormal = _spherePos - io_p->getCurrPos();
    sphereNormal.normalize();

    if(collisionDistance < 0)
    {
      //if penetrate the sphere object reflect it, compute the new position and velocity
      newP += io_p->getCurrPos();
      newV = (oldV - 1.0*(oldV.dot(sphereNormal)*sphereNormal));
      io_p->setPos(newP);
      io_p->setVelo(newV);
    }
  }
}
