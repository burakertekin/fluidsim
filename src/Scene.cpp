#include "Scene.h"
//------------------------------------------------------------------------------------------------------------
/// @file Scene.cpp
/// @author Burak Ertekin
/// @version 1.0
/// @date 19/08/15
/// @brief Scene class source.
//------------------------------------------------------------------------------------------------------------

//set the simulation box according to the given .obj input
void Scene::setSimulationBox(ngl::Obj * io_obj)
{
  ngl::BBox _bb = io_obj->getBBox();
  m_simBox.m_maxx = _bb.maxX();
  m_simBox.m_minx = _bb.minX();
  m_simBox.m_maxy = _bb.maxY();
  m_simBox.m_miny = _bb.minY();
  m_simBox.m_maxz = _bb.maxZ();
  m_simBox.m_minz = _bb.minZ();
}

//initialisation function of scene, adding boundary walls
void Scene::init()
{
  //cube only simulation box
  float _size = (m_simBox.m_maxx+m_simBox.m_maxy+m_simBox.m_maxz)/3 - (m_simBox.m_minx+m_simBox.m_miny+m_simBox.m_minz)/3;
  //time to add the walls
  //left wall
  addWall(ngl::Vec3(m_simBox.m_minx,0,0), _size, ngl::Vec3(1.0, 0.0, 0.0),true);
  //right wall
  addWall(ngl::Vec3(m_simBox.m_maxx,0,0), _size, ngl::Vec3(-1.0, 0.0, 0.0),true);
  //bottom wall
  addWall(ngl::Vec3(0,m_simBox.m_maxy,0), _size, ngl::Vec3(0.0, -1.0, 0.0),true);
  //top wall
  addWall(ngl::Vec3(0,m_simBox.m_miny,0), _size, ngl::Vec3(0.0, 1.0, 0.0),true);
  //front wall
  addWall(ngl::Vec3(0,0,m_simBox.m_maxz), _size, ngl::Vec3(0.0, 0.0, -1.0),false);
  //back wall
  addWall(ngl::Vec3(0,0,m_simBox.m_minz), _size, ngl::Vec3(0.0, 0.0, 1.0),true);
}

//add wall function
void Scene::addWall(ngl::Vec3 io_point, float io_size, ngl::Vec3 io_normal, bool io_draw)
{
  //OctreeAbstract reference
  Wall *newWall = new Wall;
  io_normal.normalize();
  newWall->centre = io_point;
  newWall->size = io_size;
  newWall->a = io_normal.m_x;
  newWall->b = io_normal.m_y;
  newWall->c = io_normal.m_z;
  newWall->d = -(newWall->a * io_point.m_x + newWall->b * io_point.m_y + newWall->c * io_point.m_z);
  newWall->draw_flag = io_draw;

  m_walls.push_back(newWall);
}

//clear the walls
void Scene::clearWalls()
{
  BOOST_FOREACH(Wall *w, m_walls)
  {
    delete w;
  }
  m_walls.clear();
}
