/********************************************************************************
** Form generated from reading UI file 'MainWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.3.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QWidget *layoutWidget;
    QGridLayout *screen;
    QPushButton *runButton;
    QPushButton *resetButton;
    QCheckBox *openglCheckBox;
    QCheckBox *collisionCheckBox;
    QRadioButton *nonNewtonianRadio;
    QRadioButton *newtonianRadio;
    QLabel *label;
    QPushButton *fileBrowse_2;
    QTabWidget *fluidTypeTab;
    QWidget *NewtonianTab_2;
    QLabel *label_3;
    QDoubleSpinBox *newGasConstantBox;
    QLabel *label_4;
    QDoubleSpinBox *newViscoBox;
    QLabel *label_5;
    QDoubleSpinBox *newVolumeBox;
    QLabel *label_6;
    QDoubleSpinBox *newTensionBox;
    QLabel *label_7;
    QDoubleSpinBox *newThresholdBox;
    QLabel *label_8;
    QDoubleSpinBox *newSmoothBox;
    QDoubleSpinBox *newDensityBox;
    QLabel *label_15;
    QWidget *NonNewtonianTab_2;
    QLabel *label_9;
    QLabel *label_10;
    QLabel *label_11;
    QDoubleSpinBox *nonNewGastConstantBox;
    QDoubleSpinBox *nonNewVolumeBox;
    QDoubleSpinBox *nonNewViscoBox;
    QLabel *label_12;
    QDoubleSpinBox *nonNewElasticityBox;
    QLabel *label_13;
    QDoubleSpinBox *nonNewRelaxationBox;
    QLabel *label_14;
    QDoubleSpinBox *nonNewSmoothBox;
    QLabel *label_16;
    QDoubleSpinBox *nonNewDensityBox;
    QTextEdit *simBoxPath;
    QLabel *label_2;
    QTextEdit *fluidObjPath;
    QPushButton *fileBrowse;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1000, 600);
        MainWindow->setLayoutDirection(Qt::LeftToRight);
        MainWindow->setAutoFillBackground(true);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        layoutWidget = new QWidget(centralwidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(8, 8, 721, 581));
        screen = new QGridLayout(layoutWidget);
        screen->setObjectName(QStringLiteral("screen"));
        screen->setSizeConstraint(QLayout::SetNoConstraint);
        screen->setContentsMargins(0, 0, 0, 0);
        runButton = new QPushButton(centralwidget);
        runButton->setObjectName(QStringLiteral("runButton"));
        runButton->setEnabled(true);
        runButton->setGeometry(QRect(770, 550, 93, 27));
        runButton->setCheckable(true);
        runButton->setChecked(false);
        runButton->setDefault(false);
        resetButton = new QPushButton(centralwidget);
        resetButton->setObjectName(QStringLiteral("resetButton"));
        resetButton->setGeometry(QRect(870, 550, 93, 27));
        openglCheckBox = new QCheckBox(centralwidget);
        openglCheckBox->setObjectName(QStringLiteral("openglCheckBox"));
        openglCheckBox->setGeometry(QRect(740, 520, 121, 21));
        openglCheckBox->setCheckable(true);
        openglCheckBox->setChecked(true);
        collisionCheckBox = new QCheckBox(centralwidget);
        collisionCheckBox->setObjectName(QStringLiteral("collisionCheckBox"));
        collisionCheckBox->setGeometry(QRect(850, 520, 151, 20));
        nonNewtonianRadio = new QRadioButton(centralwidget);
        nonNewtonianRadio->setObjectName(QStringLiteral("nonNewtonianRadio"));
        nonNewtonianRadio->setGeometry(QRect(750, 470, 131, 22));
        newtonianRadio = new QRadioButton(centralwidget);
        newtonianRadio->setObjectName(QStringLiteral("newtonianRadio"));
        newtonianRadio->setGeometry(QRect(750, 440, 108, 22));
        newtonianRadio->setChecked(true);
        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(750, 30, 231, 21));
        fileBrowse_2 = new QPushButton(centralwidget);
        fileBrowse_2->setObjectName(QStringLiteral("fileBrowse_2"));
        fileBrowse_2->setGeometry(QRect(950, 110, 31, 31));
        fluidTypeTab = new QTabWidget(centralwidget);
        fluidTypeTab->setObjectName(QStringLiteral("fluidTypeTab"));
        fluidTypeTab->setGeometry(QRect(750, 160, 231, 261));
        fluidTypeTab->setMovable(false);
        NewtonianTab_2 = new QWidget();
        NewtonianTab_2->setObjectName(QStringLiteral("NewtonianTab_2"));
        label_3 = new QLabel(NewtonianTab_2);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(10, 160, 131, 17));
        newGasConstantBox = new QDoubleSpinBox(NewtonianTab_2);
        newGasConstantBox->setObjectName(QStringLiteral("newGasConstantBox"));
        newGasConstantBox->setGeometry(QRect(20, 30, 71, 21));
        newGasConstantBox->setDecimals(1);
        newGasConstantBox->setValue(10);
        label_4 = new QLabel(NewtonianTab_2);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(10, 60, 101, 17));
        newViscoBox = new QDoubleSpinBox(NewtonianTab_2);
        newViscoBox->setObjectName(QStringLiteral("newViscoBox"));
        newViscoBox->setGeometry(QRect(20, 80, 61, 21));
        newViscoBox->setDecimals(0);
        newViscoBox->setMaximum(10000);
        newViscoBox->setValue(300);
        label_5 = new QLabel(NewtonianTab_2);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(150, 10, 51, 17));
        newVolumeBox = new QDoubleSpinBox(NewtonianTab_2);
        newVolumeBox->setObjectName(QStringLiteral("newVolumeBox"));
        newVolumeBox->setGeometry(QRect(140, 30, 69, 21));
        newVolumeBox->setDecimals(1);
        newVolumeBox->setMaximum(10000);
        newVolumeBox->setValue(21);
        label_6 = new QLabel(NewtonianTab_2);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(130, 60, 91, 17));
        newTensionBox = new QDoubleSpinBox(NewtonianTab_2);
        newTensionBox->setObjectName(QStringLiteral("newTensionBox"));
        newTensionBox->setGeometry(QRect(140, 80, 69, 21));
        newTensionBox->setDecimals(0);
        newTensionBox->setMaximum(10000);
        newTensionBox->setValue(300);
        label_7 = new QLabel(NewtonianTab_2);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(10, 110, 111, 17));
        newThresholdBox = new QDoubleSpinBox(NewtonianTab_2);
        newThresholdBox->setObjectName(QStringLiteral("newThresholdBox"));
        newThresholdBox->setGeometry(QRect(20, 130, 69, 21));
        newThresholdBox->setDecimals(1);
        newThresholdBox->setValue(4);
        label_8 = new QLabel(NewtonianTab_2);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(10, 10, 91, 17));
        newSmoothBox = new QDoubleSpinBox(NewtonianTab_2);
        newSmoothBox->setObjectName(QStringLiteral("newSmoothBox"));
        newSmoothBox->setGeometry(QRect(20, 180, 69, 21));
        newSmoothBox->setDecimals(1);
        newSmoothBox->setValue(0.3);
        newDensityBox = new QDoubleSpinBox(NewtonianTab_2);
        newDensityBox->setObjectName(QStringLiteral("newDensityBox"));
        newDensityBox->setGeometry(QRect(140, 130, 66, 21));
        newDensityBox->setDecimals(1);
        newDensityBox->setMaximum(10000);
        newDensityBox->setValue(998.2);
        label_15 = new QLabel(NewtonianTab_2);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setGeometry(QRect(140, 110, 81, 16));
        fluidTypeTab->addTab(NewtonianTab_2, QString());
        NonNewtonianTab_2 = new QWidget();
        NonNewtonianTab_2->setObjectName(QStringLiteral("NonNewtonianTab_2"));
        label_9 = new QLabel(NonNewtonianTab_2);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(10, 10, 91, 17));
        label_10 = new QLabel(NonNewtonianTab_2);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(150, 10, 51, 17));
        label_11 = new QLabel(NonNewtonianTab_2);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(10, 60, 101, 17));
        nonNewGastConstantBox = new QDoubleSpinBox(NonNewtonianTab_2);
        nonNewGastConstantBox->setObjectName(QStringLiteral("nonNewGastConstantBox"));
        nonNewGastConstantBox->setGeometry(QRect(20, 30, 71, 21));
        nonNewGastConstantBox->setDecimals(1);
        nonNewGastConstantBox->setMaximum(10000);
        nonNewGastConstantBox->setValue(10);
        nonNewVolumeBox = new QDoubleSpinBox(NonNewtonianTab_2);
        nonNewVolumeBox->setObjectName(QStringLiteral("nonNewVolumeBox"));
        nonNewVolumeBox->setGeometry(QRect(140, 30, 69, 21));
        nonNewVolumeBox->setDecimals(1);
        nonNewVolumeBox->setMaximum(10000);
        nonNewVolumeBox->setValue(21);
        nonNewViscoBox = new QDoubleSpinBox(NonNewtonianTab_2);
        nonNewViscoBox->setObjectName(QStringLiteral("nonNewViscoBox"));
        nonNewViscoBox->setGeometry(QRect(20, 80, 61, 21));
        nonNewViscoBox->setDecimals(0);
        nonNewViscoBox->setMaximum(10000);
        nonNewViscoBox->setValue(300);
        label_12 = new QLabel(NonNewtonianTab_2);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(10, 110, 101, 17));
        nonNewElasticityBox = new QDoubleSpinBox(NonNewtonianTab_2);
        nonNewElasticityBox->setObjectName(QStringLiteral("nonNewElasticityBox"));
        nonNewElasticityBox->setGeometry(QRect(20, 130, 69, 21));
        nonNewElasticityBox->setDecimals(0);
        nonNewElasticityBox->setMaximum(1e+07);
        nonNewElasticityBox->setValue(1000);
        label_13 = new QLabel(NonNewtonianTab_2);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(120, 60, 111, 17));
        nonNewRelaxationBox = new QDoubleSpinBox(NonNewtonianTab_2);
        nonNewRelaxationBox->setObjectName(QStringLiteral("nonNewRelaxationBox"));
        nonNewRelaxationBox->setGeometry(QRect(140, 80, 69, 21));
        nonNewRelaxationBox->setDecimals(1);
        nonNewRelaxationBox->setMaximum(100);
        nonNewRelaxationBox->setValue(0.1);
        label_14 = new QLabel(NonNewtonianTab_2);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setGeometry(QRect(10, 160, 141, 17));
        nonNewSmoothBox = new QDoubleSpinBox(NonNewtonianTab_2);
        nonNewSmoothBox->setObjectName(QStringLiteral("nonNewSmoothBox"));
        nonNewSmoothBox->setGeometry(QRect(20, 180, 69, 21));
        nonNewSmoothBox->setDecimals(1);
        nonNewSmoothBox->setValue(0.3);
        label_16 = new QLabel(NonNewtonianTab_2);
        label_16->setObjectName(QStringLiteral("label_16"));
        label_16->setGeometry(QRect(130, 110, 91, 16));
        nonNewDensityBox = new QDoubleSpinBox(NonNewtonianTab_2);
        nonNewDensityBox->setObjectName(QStringLiteral("nonNewDensityBox"));
        nonNewDensityBox->setGeometry(QRect(140, 130, 66, 21));
        nonNewDensityBox->setDecimals(1);
        nonNewDensityBox->setMaximum(10000);
        nonNewDensityBox->setValue(998.2);
        fluidTypeTab->addTab(NonNewtonianTab_2, QString());
        simBoxPath = new QTextEdit(centralwidget);
        simBoxPath->setObjectName(QStringLiteral("simBoxPath"));
        simBoxPath->setGeometry(QRect(750, 50, 191, 31));
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(750, 90, 241, 21));
        fluidObjPath = new QTextEdit(centralwidget);
        fluidObjPath->setObjectName(QStringLiteral("fluidObjPath"));
        fluidObjPath->setGeometry(QRect(750, 110, 191, 31));
        fluidObjPath->setOverwriteMode(false);
        fileBrowse = new QPushButton(centralwidget);
        fileBrowse->setObjectName(QStringLiteral("fileBrowse"));
        fileBrowse->setGeometry(QRect(950, 50, 31, 31));
        MainWindow->setCentralWidget(centralwidget);

        retranslateUi(MainWindow);

        fluidTypeTab->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        runButton->setText(QApplication::translate("MainWindow", "Run/Pause", 0));
        resetButton->setText(QApplication::translate("MainWindow", "Reset", 0));
        openglCheckBox->setText(QApplication::translate("MainWindow", "Draw Scene", 0));
        collisionCheckBox->setText(QApplication::translate("MainWindow", "Collision Condition", 0));
        nonNewtonianRadio->setText(QApplication::translate("MainWindow", "Non-Newtonian", 0));
        newtonianRadio->setText(QApplication::translate("MainWindow", "Newtonian", 0));
        label->setText(QApplication::translate("MainWindow", "Simulation Box Path", 0));
        fileBrowse_2->setText(QApplication::translate("MainWindow", "...", 0));
        label_3->setText(QApplication::translate("MainWindow", "Smoothing Length", 0));
        label_4->setText(QApplication::translate("MainWindow", "Viscosity Coeff.", 0));
        label_5->setText(QApplication::translate("MainWindow", "Volume", 0));
        label_6->setText(QApplication::translate("MainWindow", "Tension Coeff.", 0));
        label_7->setText(QApplication::translate("MainWindow", "Surface Threshold", 0));
        label_8->setText(QApplication::translate("MainWindow", "Gas Constant", 0));
        label_15->setText(QApplication::translate("MainWindow", "Rest Density", 0));
        fluidTypeTab->setTabText(fluidTypeTab->indexOf(NewtonianTab_2), QApplication::translate("MainWindow", "Newtonian", 0));
        label_9->setText(QApplication::translate("MainWindow", "Gas Constant", 0));
        label_10->setText(QApplication::translate("MainWindow", "Volume", 0));
        label_11->setText(QApplication::translate("MainWindow", "Viscosity Coeff.", 0));
        label_12->setText(QApplication::translate("MainWindow", "Elasticity Cons.", 0));
        label_13->setText(QApplication::translate("MainWindow", "Relaxation Time", 0));
        label_14->setText(QApplication::translate("MainWindow", "Smoothing Length", 0));
        label_16->setText(QApplication::translate("MainWindow", "Rest Density", 0));
        fluidTypeTab->setTabText(fluidTypeTab->indexOf(NonNewtonianTab_2), QApplication::translate("MainWindow", "Non-Newtonian", 0));
        simBoxPath->setPlaceholderText(QString());
        label_2->setText(QApplication::translate("MainWindow", "Fluid Object Path", 0));
        fluidObjPath->setPlaceholderText(QString());
        fileBrowse->setText(QApplication::translate("MainWindow", "...", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
